﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CommunicatorWhitServer
{
    static class Communicator
    {
        static Socket m_socket = null;

        public static void SocketConnect()
        {
            try
            {
                byte[] bytes = new byte[1024];
                IPHostEntry ipHost = Dns.GetHostEntry("localhost");
                IPAddress ipAddr = ipHost.AddressList[1];
                IPEndPoint remoteEP = new IPEndPoint(ipAddr, 8820);
                m_socket = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                m_socket.Connect(remoteEP);
                Console.WriteLine("Socket connected to " + m_socket.RemoteEndPoint.ToString());
            }
            catch (Exception ex)
            {

                Console.WriteLine("An error occurred: " + ex.Message);
            }
        
        }

        public static void sendMessage(string message)
        {
            byte[] msg = Encoding.ASCII.GetBytes(message);
            int bytesSent = m_socket.Send(msg);
        }

        public static string recvMessage()
        {
            byte[] bytes = new byte[1024];
            int bytesRec = m_socket.Receive(bytes);
            return Encoding.ASCII.GetString(bytes, 0, bytesRec);
        }

        public static void SocketDisconnect()
        {
            //m_socket.Disconnect(false);
            m_socket.Close();
        }
    }
}
