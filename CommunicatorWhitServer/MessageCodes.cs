﻿
public  enum MessageCodes
{
    ERROR_CODE = 100,
    LOGIN_CODE = 101,
    SIGNUP_CODE = 102,
    LOGOUT_CODE = 103,
    GET_ROOMS_CODE = 201,
    PLAYERS_IN_ROOM_CODE = 202,
    HIGH_SCORE_CODE = 203,
    PERSONAL_STATS_CODE = 204,
    JOIN_ROOM_CODE = 205,
    CREATE_ROOM_CODE = 206
};