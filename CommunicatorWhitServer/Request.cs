﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace CommunicatorWhitServer
{
    internal class Request
    {
        public static void login(string username, string password)
        {
            var credentials = new
            {
                username = username,
                password = password
            };

            // Serialize the anonymous object to JSON
            string msgJSON = JsonSerializer.Serialize(credentials);

            // Output the JSON string
            //Console.WriteLine(msgJSON);
            string size = (Encoding.UTF8.GetByteCount(msgJSON)).ToString();
            padding(ref size);

            //Console.WriteLine("Size of msgJSON: " + size + " bytes");

            Communicator.sendMessage(((int)(MessageCodes.LOGIN_CODE)).ToString() + size + msgJSON);
        }

        public static void singup(string username, string password, string email)
        {
            var credentials = new
            {
                username = username,
                password = password,
                email=email
            };

            // Serialize the anonymous object to JSON
            string msgJSON = JsonSerializer.Serialize(credentials);

            // Output the JSON string
            //Console.WriteLine(msgJSON);
            string size = (Encoding.UTF8.GetByteCount(msgJSON)).ToString();
            padding(ref size);

            //Console.WriteLine("Size of msgJSON: " + size + " bytes");

            Communicator.sendMessage(((int)(MessageCodes.SIGNUP_CODE)).ToString() + size + msgJSON);
        }

        public static void logout()
        {
            var credentials = new
            {
                
            };

            // Serialize the anonymous object to JSON
            string msgJSON = JsonSerializer.Serialize(credentials);

            // Output the JSON string
            //Console.WriteLine(msgJSON);
            string size = (Encoding.UTF8.GetByteCount(msgJSON)).ToString();
            padding(ref size);

            //Console.WriteLine("Size of msgJSON: " + size + " bytes");

            Communicator.sendMessage(((int)(MessageCodes.LOGOUT_CODE)).ToString() + size + msgJSON);
        }


        public static void getRooms()
        {
            var credentials = new
            {

            };

            // Serialize the anonymous object to JSON
            string msgJSON = JsonSerializer.Serialize(credentials);

            // Output the JSON string
            //Console.WriteLine(msgJSON);
            string size = (Encoding.UTF8.GetByteCount(msgJSON)).ToString();
            padding(ref size);

            //Console.WriteLine("Size of msgJSON: " + size + " bytes");

            Communicator.sendMessage(((int)(MessageCodes.GET_ROOMS_CODE)).ToString() + size + msgJSON);
        }

        public static void getPlayersInTheRoom(int roomId)
        {
            var credentials = new
            {
                roomId=roomId
            };

            // Serialize the anonymous object to JSON
            string msgJSON = JsonSerializer.Serialize(credentials);

            // Output the JSON string
            //Console.WriteLine(msgJSON);
            string size = (Encoding.UTF8.GetByteCount(msgJSON)).ToString();
            padding(ref size);

            //Console.WriteLine("Size of msgJSON: " + size + " bytes");

            Communicator.sendMessage(((int)(MessageCodes.PLAYERS_IN_ROOM_CODE)).ToString() + size + msgJSON);
        }

        public static void highScore()
        {
            var credentials = new
            {

            };

            // Serialize the anonymous object to JSON
            string msgJSON = JsonSerializer.Serialize(credentials);

            // Output the JSON string
            //Console.WriteLine(msgJSON);
            string size = (Encoding.UTF8.GetByteCount(msgJSON)).ToString();
            padding(ref size);

            //Console.WriteLine("Size of msgJSON: " + size + " bytes");

            Communicator.sendMessage(((int)(MessageCodes.HIGH_SCORE_CODE)).ToString() + size + msgJSON);
        }

        public static void personalStats()
        {
            var credentials = new
            {

            };

            // Serialize the anonymous object to JSON
            string msgJSON = JsonSerializer.Serialize(credentials);

            // Output the JSON string
            //Console.WriteLine(msgJSON);
            string size = (Encoding.UTF8.GetByteCount(msgJSON)).ToString();
            padding(ref size);

            //Console.WriteLine("Size of msgJSON: " + size + " bytes");

            Communicator.sendMessage(((int)(MessageCodes.PERSONAL_STATS_CODE)).ToString() + size + msgJSON);
        }


        public static void joinRoom(int roomId)
        {
            var credentials = new
            {
                roomId =roomId
            };

            // Serialize the anonymous object to JSON
            string msgJSON = JsonSerializer.Serialize(credentials);

            // Output the JSON string
            //Console.WriteLine(msgJSON);
            string size = (Encoding.UTF8.GetByteCount(msgJSON)).ToString();
            padding(ref size);

            //Console.WriteLine("Size of msgJSON: " + size + " bytes");

            Communicator.sendMessage(((int)(MessageCodes.JOIN_ROOM_CODE)).ToString() + size + msgJSON);
        }


        public static void createRoom(string roomName,int MaxUser,int questionCount,int answerTimeout)
        {
            var credentials = new
            {
                roomName=roomName,
                MaxUser=MaxUser,
                questionCount=questionCount,
                answerTimeout=answerTimeout
            };

            // Serialize the anonymous object to JSON
            string msgJSON = JsonSerializer.Serialize(credentials);

            // Output the JSON string
            //Console.WriteLine(msgJSON);
            string size = (Encoding.UTF8.GetByteCount(msgJSON)).ToString();
            padding(ref size);

            //Console.WriteLine("Size of msgJSON: " + size + " bytes");

            Communicator.sendMessage(((int)(MessageCodes.JOIN_ROOM_CODE)).ToString() + size + msgJSON);
        }
        private static void  padding(ref string length)
        {
             length = "0".PadLeft(4 - Math.Min(4, length.Length), '0') + length;
        }
    }
}
