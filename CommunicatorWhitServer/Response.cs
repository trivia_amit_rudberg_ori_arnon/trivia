﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace CommunicatorWhitServer
{
    internal class Response
    {
        public static int getStatus(string message)
        {
            int braceIndex = message.IndexOf('{');

            // Get the substring starting from the opening curly brace
            string jsonSubstring = message.Substring(braceIndex);



            // Find the index of the "status" key
            int statusIndex = jsonSubstring.IndexOf("status");

            // Find the index of the colon after the "status" key
            int colonIndex = jsonSubstring.IndexOf(':', statusIndex);


            // Find the index of the closing curly brace after the colon
            int closingBraceIndex = jsonSubstring.IndexOf('}', colonIndex);

            string statusSubstring = jsonSubstring.Substring(colonIndex + 1, closingBraceIndex - colonIndex - 1).Trim();


            if (!int.TryParse(statusSubstring, out int status))
            {
                // Return -1 if status value cannot be parsed
                return -1;
            }

            // Return the parsed status value
            return status;
        }

        public static List<string> GetRooms(string message)
        {
            List<string> rooms = new List<string>();

            // Create a new list to store room names
            // Find the index of the opening curly brace of the JSON substring
            int braceIndex = message.IndexOf('{');
            if (braceIndex == -1)
            {
                // Return an empty list if opening curly brace not found
                return rooms;
            }

            // Get the substring starting from the opening curly brace
            string jsonSubstring = message.Substring(braceIndex);

            // Try to parse the JSON substring
            try
            {
                // Parse the JSON substring into a JsonDocument
                JsonDocument jsonDocument = JsonDocument.Parse(jsonSubstring);

                // Get the root element of the JSON document
                JsonElement root = jsonDocument.RootElement;

                // Get the "rooms" property
                JsonElement roomsElement = root.GetProperty("rooms");

                // Check if the "rooms" property is an array
                if (roomsElement.ValueKind == JsonValueKind.Array)
                {
                    // Iterate over the array and extract room names
                    foreach (JsonElement roomElement in roomsElement.EnumerateArray())
                    {
                        rooms.Add(roomElement.GetString());
                    }
                }
            }
            catch (JsonException)
            {
                // Handle JSON parsing error
                Console.WriteLine("Error parsing JSON");
            }

            return rooms;
        }


        public static List<string> GetPlayersInRoom(string message)
        {
            List<string> rooms = new List<string>();

            // Create a new list to store room names
            // Find the index of the opening curly brace of the JSON substring
            int braceIndex = message.IndexOf('{');
            if (braceIndex == -1)
            {
                // Return an empty list if opening curly brace not found
                return rooms;
            }

            // Get the substring starting from the opening curly brace
            string jsonSubstring = message.Substring(braceIndex);

            // Try to parse the JSON substring
            try
            {
                // Parse the JSON substring into a JsonDocument
                JsonDocument jsonDocument = JsonDocument.Parse(jsonSubstring);

                // Get the root element of the JSON document
                JsonElement root = jsonDocument.RootElement;

                // Get the "rooms" property
                JsonElement roomsElement = root.GetProperty("players");

                // Check if the "rooms" property is an array
                if (roomsElement.ValueKind == JsonValueKind.Array)
                {
                    // Iterate over the array and extract room names
                    foreach (JsonElement roomElement in roomsElement.EnumerateArray())
                    {
                        rooms.Add(roomElement.GetString());
                    }
                }
            }
            catch (JsonException)
            {
                // Handle JSON parsing error
                Console.WriteLine("Error parsing JSON");
            }

            return rooms;
        }


        public static List<string> getStatistics(string message)
        {
            List<string> rooms = new List<string>();

            // Create a new list to store room names
            // Find the index of the opening curly brace of the JSON substring
            int braceIndex = message.IndexOf('{');
            if (braceIndex == -1)
            {
                // Return an empty list if opening curly brace not found
                return rooms;
            }

            // Get the substring starting from the opening curly brace
            string jsonSubstring = message.Substring(braceIndex);

            // Try to parse the JSON substring
            try
            {
                // Parse the JSON substring into a JsonDocument
                JsonDocument jsonDocument = JsonDocument.Parse(jsonSubstring);

                // Get the root element of the JSON document
                JsonElement root = jsonDocument.RootElement;

                // Get the "rooms" property
                JsonElement roomsElement = root.GetProperty("statistics");

                // Check if the "rooms" property is an array
                if (roomsElement.ValueKind == JsonValueKind.Array)
                {
                    // Iterate over the array and extract room names
                    foreach (JsonElement roomElement in roomsElement.EnumerateArray())
                    {
                        rooms.Add(roomElement.GetString());
                    }
                }
            }
            catch (JsonException)
            {
                // Handle JSON parsing error
                Console.WriteLine("Error parsing JSON");
            }

            return rooms;
        }
    }
}
