#pragma once
#include <vector>
#include <map>
#include <string>
#include "iostream"
#include "Question.h"
#include "LoggedUser.h"
#include "GameData.h"
#include "IDatabase.h"
#include <chrono>
#include <iterator>
using std::string;
class Game
{
public:
	Game(unsigned int gameId,unsigned int amountOfQuestions, IDatabase* database, std::vector<LoggedUser>& players);
	

	Question& getQuestionForUser(LoggedUser& user);
	bool submitAnswer(LoggedUser& user, int answerId);
	void removePlayer(LoggedUser& user);
	unsigned int getGameId();
	std::map<LoggedUser, GameData>& getPlayerMap();
	void stopClock(LoggedUser& user);
	
private:
	IDatabase* m_dataBase;
	std::vector<Question> m_questions;
	std::map<LoggedUser, GameData> m_players;
	unsigned int  m_gameId;
	std::chrono::time_point<std::chrono::high_resolution_clock> timeTheGameStart;

	void submitGameStatsToDB(LoggedUser loggeduser,GameData gameData,IDatabase* dataBase);
};

