#pragma once
#include <string>
#include "Question.h"
#include "GameData.h"
#include "LoggedUser.h"
#include <list>
using std::string;
class IDatabase
{
public:
	virtual bool open() = 0;
	virtual bool close()  = 0;
	virtual int doesUserExist(string User) = 0;
	virtual int doesPasswordMatch(string User,string Password) = 0;
	virtual int addNewUser(string User,string Password,string mail) = 0;
	virtual std::list<Question> getQuestions(int amount) = 0;
	virtual std::vector<string> getHighScores()=0;
	virtual int getNumOfCorrectAnswers(std::string user)=0;
	virtual int getNumOfPlayerGames(std::string user)=0;
	virtual int getNumOfTotalAnswers(std::string user) = 0;
	virtual double getPlayerAverageTime(std::string user)=0;
	virtual int getPlayerScore(std::string user) = 0;
	virtual void submitGameStatistics(LoggedUser loggeduser, GameData gameData) = 0;
private:

};
