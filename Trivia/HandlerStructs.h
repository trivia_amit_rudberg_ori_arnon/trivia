#ifndef HANDLERSTRUCTS_H
#define HANDLERSTRUCTS_H
#include <string>
#include <ctime>
#include <vector>
#include "IRequestHandler.h"

class IRequestHandler;

struct RequestResult
{
	std::vector<unsigned char> response;
	IRequestHandler* newHandler;
};

struct RequestInfo
{
	unsigned int id;
	std::time_t recivalTime;
	std::vector<unsigned char> buffer;
};
#endif