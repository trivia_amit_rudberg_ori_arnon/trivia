#include "Room.h"

void Room::addUser(LoggedUser& user)
{
	m_users.push_back(user);
}

void Room::removeUser(LoggedUser user)
{
	for (auto it = m_users.begin(); it != m_users.end(); it++)
	{
		if (it->getUsername() == user.getUsername())
		{
			m_users.erase(it);
			break;
		}
	}
}

std::vector<LoggedUser>& Room::getAllUsers()
{
	return m_users;

	/*std::vector<std::string> users;
	for (auto it = m_users.begin(); it != m_users.end(); it++)
	{
		users.push_back(it->getUsername());
	}
	
	return users;*/
}

void Room::setRoomData(RoomData roomData)
{
	m_metadata = roomData;
}

RoomData& Room::getRoomData()
{
	return m_metadata;
}

