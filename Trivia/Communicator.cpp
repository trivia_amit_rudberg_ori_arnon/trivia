#include "Communicator.h"
#include "MessageCodes.h"
#include "HandlerStructs.h"
#include <ctime>

#pragma warning(disable : 4996)

Communicator::Communicator(RequestHandlerFactory& factory) : m_handlerFactory(factory)
{
	m_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (m_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");
}

Communicator::~Communicator()
{
	try
	{
		if (m_serverSocket != INVALID_SOCKET) {
			closesocket(m_serverSocket);
		}
	}
	catch (const std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
	
}

void Communicator::startHandleRequests()
{
	bindAndListen();
}

void Communicator::bindAndListen()
{
	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(PORT);
	sa.sin_family = AF_INET;
	sa.sin_addr.s_addr = INADDR_ANY;

	if (bind(m_serverSocket, (struct sockaddr*)&sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	if (listen(m_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << PORT << std::endl;

	while (true)
	{
		SOCKET client_socket = accept(m_serverSocket, NULL, NULL);
		if (client_socket == INVALID_SOCKET)
			throw std::exception(__FUNCTION__);

		std::cout << "Client '" << client_socket << "'accepted" << std::endl;

		std::thread currentClient(&Communicator::handleNewClient, this, client_socket);
		currentClient.detach();

		m_clients.insert(std::pair<SOCKET, IRequestHandler*>(client_socket, (IRequestHandler*)(new LoginRequestHandler(m_handlerFactory))));
	}
}

void Communicator::handleNewClient(SOCKET sck)
{
	char* data = new char[1024];
	bool connected = true;
	SSIZE_T length = 0;
	RequestInfo ri;
	//std::string prev = "";

	while (connected)
	{
		try
		{
			if (recv(sck, data, 1024, 0) == INVALID_SOCKET)
			{
				sendError("error reciving message", sck);
				std::cout << "error reciving message from connection '" << sck << "', sending error" << std::endl;
				connected = false;
			}
			else
			{
				ri.id = getRequestId(data);
				int dataLength = std::stoi(std::string(data).substr(MESSAGE_CODE_LENGTH, 4));
				//if (dataLength > 1024) { throw std::exception("message too long"); }
				ri.buffer = std::vector<unsigned char>(data, data + dataLength + 4 + MESSAGE_CODE_LENGTH);
				ri.recivalTime = std::time(NULL);
				std::memset(data, 0, sizeof(data));
				std::cout << "request recived from connection '" << sck << "': " << std::string(ri.buffer.begin(), ri.buffer.end()) << std::endl;
				RequestResult result = (*m_clients.find(sck)).second->handleRequest(ri);
				if (result.newHandler == nullptr)
				{
					sendError("error handeling request", sck);
					std::cout << "error handeling request from connection '" << sck << "', sending error" << std::endl;
					connected = false;
				}
				else
				{
					std::cout << "sending response to connection '" << sck << "': " << std::string(result.response.begin(), result.response.end()) << std::endl;
					send(sck, std::string(result.response.begin(), result.response.end()).c_str(), result.response.size(), 0);

					(*m_clients.find(sck)).second = result.newHandler;
				}
			}
		}
		catch (const std::exception& e)
		{
			std::cout << "error in client '" << sck << "': " << e.what() << std::endl;
			connected = false;
		}
	}
	std::cout << "client '" << sck << "' disconnected" << std::endl;
}

void Communicator::sendError(std::string message, SOCKET sck)
{
	ErrorResponse response;
	response.message = message;

	std::vector<unsigned char> buffer = JsonResponsePacketSerializer::SerializeResponse(response);
	send(sck, std::string(buffer.begin(), buffer.end()).c_str(), buffer.size(), 0);
}

unsigned int Communicator::getRequestId(char codeArr[MESSAGE_CODE_LENGTH])
{
	std::string code;
	for (int i = 0; i < MESSAGE_CODE_LENGTH; i++)
	{
		code += codeArr[i];
	}
	return std::stoi(code);
}