#include "RoomMemberRequestHandler.h"

RoomMemberRequestHandler::RoomMemberRequestHandler(RequestHandlerFactory& factory, LoggedUser& user, Room& room) : m_user(user), m_handlerFactory(factory), m_room(room) {}

RoomMemberRequestHandler::~RoomMemberRequestHandler() {};

bool RoomMemberRequestHandler::isRequestRelevant(RequestInfo& info)
{
	return info.id == MessageCodes::LEAVE_ROOM_CODE || info.id == MessageCodes::ROOM_STATE_CODE;
}

RequestResult RoomMemberRequestHandler::handleRequest(RequestInfo& info)
{
	RequestResult result;

	if (!isRequestRelevant(info))
	{
		result.newHandler = nullptr;
		return result;
	}

	switch (info.id)
	{
	case MessageCodes::LEAVE_ROOM_CODE:
		result = leaveRoom(info);
		break;
	case MessageCodes::ROOM_STATE_CODE:
		result = getRoomState(info);
		break;
	default:
		break;
	}

	return result;
}


RequestResult RoomMemberRequestHandler::leaveRoom(RequestInfo info)
{
	RequestResult result;
	LeaveRoomResponse response;
	m_room.removeUser(m_user);
	response.status = 0;
	result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);
	result.response = JsonResponsePacketSerializer::SerializeResponse(response);
	return result;
}

RequestResult RoomMemberRequestHandler::getRoomState(RequestInfo info)
{
	RequestResult result;
	GetRoomStateResponse response;
	try
	{
		response.id = m_room.getRoomData().id;
		response.answerTimeout = m_room.getRoomData().timePerQuestion;
		response.hasGameBegun = m_room.getRoomData().isActive;
		//response.players = m_room.getAllUsers();
		for (auto i : m_room.getAllUsers())
		{
			response.players.push_back(i.getUsername());
		}
		response.questionCount = m_room.getRoomData().numOfQuestionInGame;
		response.status = 0;
		if (m_room.getRoomData().isActive)
		{
			result.newHandler = m_handlerFactory.createGameRequestHandler(m_user, m_handlerFactory.getGameManager().getGame(m_room.getRoomData().id));
		}
		else
		{
			result.newHandler = m_handlerFactory.createRoomMemberRequestHandler(m_user, m_room);
		}
	}
	catch (const std::exception& e)
	{
		//in case room is closed, the m_room refecrence wont be working
		response.status = 1;

		response.id = -99;
		response.answerTimeout = 0;
		response.hasGameBegun = false;
		response.players = std::vector<std::string>();
		response.questionCount = 0;
		result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);
	}
	result.response = JsonResponsePacketSerializer::SerializeResponse(response);
	return result;
}