#pragma once
#ifndef ROOMDATA_H
#define ROOMDATA_H
#include <string>
using std::string;

struct RoomData
{
	unsigned int id;
	string name;
	unsigned int maxPlayers;
	unsigned int numOfQuestionInGame;
	unsigned int timePerQuestion;
	unsigned int isActive;

	RoomData()
	{
		id = 0;
		name = "";
		maxPlayers = 0;
		numOfQuestionInGame = 0;
		timePerQuestion = 0;
		isActive = 0;
	}
};


#endif