#include "Game.h"

Game::Game(unsigned int gameId, unsigned int amountOfQuestions, IDatabase* database, std::vector<LoggedUser>& players) : m_dataBase(database)
{
	
	std::list<Question> q = database->getQuestions(amountOfQuestions);
	for (const auto& question : q)
	{
		m_questions.push_back(question);
	}

	for (auto& playerName : players)
	{
		GameData* gameData = new GameData();

		m_players[playerName.getUsername()] = *gameData;
		m_players[playerName.getUsername()].currentQuestion = m_questions[0];
	}
	m_gameId = gameId;
	timeTheGameStart = std::chrono::high_resolution_clock::now();
}



Question& Game::getQuestionForUser(LoggedUser& user)
{
	return m_players[user].currentQuestion;
}

bool Game::submitAnswer(LoggedUser& user,int answerId)
{
	try
	{
		bool flag;
		if (answerId == m_players[user].currentQuestion.CorrectAnswerId())
		{
			m_players[user].correctAnswerCount++;
			flag = true;//answer correct 
		}
		else
		{
			m_players[user].wrongAnswerCount++;
			flag = false;//answer wrong
		}

		m_players[user].currAnswer += (m_players[user].currAnswer < m_questions.size() - 1) ? 1 : 0;
		m_players[user].currentQuestion = m_questions[m_players[user].currAnswer];
		if (m_players[user].currAnswer == m_questions.size())
		{
			stopClock(user);
		}
		return flag;
	}
	catch (const std::exception&)
	{
		std::cout << "The user is not in the game" << std::endl;
	}
}

void Game::removePlayer(LoggedUser& user)
{
	try
	{
		if (m_players[user].currAnswer != m_questions.size())
		{
			stopClock(user);
		}
		submitGameStatsToDB(user, m_players[user], m_dataBase);
		m_players.erase(user);

	}
	catch (const std::exception&)
	{
		std::cout << "The user is not in the game" << std::endl;
	}
}

unsigned int Game::getGameId()
{
	return m_gameId;
}


void Game::submitGameStatsToDB(LoggedUser loggeduser,GameData gameData, IDatabase* dataBase)
{
	dataBase->submitGameStatistics(loggeduser,gameData);
}

std::map<LoggedUser, GameData>& Game::getPlayerMap()
{
	return m_players;
}

void Game::stopClock(LoggedUser& user)
{
	auto timeTheGameEnd = std::chrono::high_resolution_clock::now();
	auto duration = std::chrono::duration_cast<std::chrono::seconds>(timeTheGameEnd-timeTheGameStart);
	if (m_players[user].currAnswer == 0)
	{
		m_players[user].averangeAnswerTime = 0;

	}
	else
	{
		m_players[user].averangeAnswerTime = (duration.count() / m_players[user].currAnswer);
	}
}
