#include "RoomAdminRequestHandler.h"


RoomAdminRequestHandler::RoomAdminRequestHandler(RequestHandlerFactory& factory, LoggedUser& user, Room& room) : m_user(user), m_handlerFactory(factory), m_room(room) {}

RoomAdminRequestHandler::~RoomAdminRequestHandler() {};

bool RoomAdminRequestHandler::isRequestRelevant(RequestInfo& info)
{
	return info.id == MessageCodes::CLOSE_ROOM_CODE ||
		info.id == MessageCodes::START_GAME_CODE ||
		info.id == MessageCodes::ROOM_STATE_CODE;
}

RequestResult RoomAdminRequestHandler::handleRequest(RequestInfo& info)
{
	RequestResult result;

	if (!isRequestRelevant(info))
	{
		result.newHandler = nullptr;
		return result;
	}

	switch (info.id)
	{
	case MessageCodes::CLOSE_ROOM_CODE:
		result = closeRoom(info);
		break;
	case MessageCodes::START_GAME_CODE:
		result = startGame(info);
		break;
	case MessageCodes::ROOM_STATE_CODE:
		result = getRoomState(info);
		break;
	default:
		break;
	}

	return result;
}

RequestResult RoomAdminRequestHandler::closeRoom(RequestInfo info)
{
	RequestResult result;
	CloseRoomResponse response;
	response.status == 1;
	for (RoomData& rd : m_handlerFactory.getRoomManager().getRooms())
	{
		for (LoggedUser& player : m_handlerFactory.getRoomManager().getRoom(rd.id).getAllUsers())
		{
			if (player.getUsername() == m_user.getUsername())
			{
				m_handlerFactory.getRoomManager().deleteRoom(rd.id);
				response.status = 0;
				break;
			}
		}
	}

	result.response = JsonResponsePacketSerializer::SerializeResponse(response);
	if (response.status == 0) { result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user); }
	else { result.newHandler = m_handlerFactory.createRoomAdminRequestHandler(m_user, m_room); }
	return result;
}
RequestResult RoomAdminRequestHandler::startGame(RequestInfo info)
{
	RequestResult result;
	StartGameResponse response;
	RoomData rd = m_room.getRoomData();
	rd.isActive = true;
	m_room.setRoomData(rd);
	response.status = 0;
	m_handlerFactory.getGameManager().createGame(m_room);
	result.newHandler = m_handlerFactory.createGameRequestHandler(m_user, m_handlerFactory.getGameManager().getGame(m_room.getRoomData().id));
	//result.newHandler = m_handlerFactory.createGameRequestHandler(m_user, m_handlerFactory.getGameManager().createGame(m_room));
	result.response = JsonResponsePacketSerializer::SerializeResponse(response);
	return result;
}
RequestResult RoomAdminRequestHandler::getRoomState(RequestInfo info)
{
	RequestResult result;
	GetRoomStateResponse response;
	response.id = m_room.getRoomData().id;
	response.answerTimeout = m_room.getRoomData().timePerQuestion;
	response.hasGameBegun = m_room.getRoomData().isActive;
	//response.players = m_room.getAllUsers();
	for (auto i : m_room.getAllUsers())
	{
		response.players.push_back(i.getUsername());
	}
	response.questionCount = m_room.getRoomData().numOfQuestionInGame;
	response.status = 0;
	result.response = JsonResponsePacketSerializer::SerializeResponse(response);
	result.newHandler = m_handlerFactory.createRoomAdminRequestHandler(m_user, m_room);
	return result;
}