#ifndef GAMEREQUESTHANDLER_H
#define GAMEREQUESTHANDLER_H
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "GameManager.h"
#include "Game.h"

class RequestHandlerFactory;

class GameRequestHandler : public IRequestHandler
{
public:
	GameRequestHandler(RequestHandlerFactory& factory, LoggedUser& user, GameManager& gm, Game* game);
	~GameRequestHandler();

	bool isRequestRelevant(RequestInfo& info) override;
	RequestResult handleRequest(RequestInfo& info) override;
private:
	LoggedUser& m_user;
	GameManager& m_gameManager;
	RequestHandlerFactory& m_handlerFactory;
	Game* m_game;

	RequestResult getQuestion(RequestInfo& info);
	RequestResult submitAnswer(RequestInfo& info);
	RequestResult getGameResults(RequestInfo& info);
	RequestResult leaveGame(RequestInfo& info);
};
#endif