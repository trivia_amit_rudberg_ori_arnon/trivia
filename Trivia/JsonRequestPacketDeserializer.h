#ifndef JSONRESPONSEPACKETDESERIALIZER_H
#define JSONRESPONSEPACKETDESERIALIZER_H
#include "RequestStructs.h"
#include "MessageCodes.h"
#include <string>
#include <vector>

static class JsonResponsePacketDeserializer
{
public:
	static LoginRequest deserializeLoginRequest(std::vector<unsigned char> buffer);
	static SignUpRequest deserializeSignUpRequest(std::vector<unsigned char> buffer);

	static GetPlayersInRoomRequest deserializeGetPlayersRequest(std::vector<unsigned char> buffer);
	static JoinRoomRequest deserializeJoinRoomRequest(std::vector<unsigned char> buffer);
	static CreateRoomRequest deserializeCreateRoomRequest(std::vector<unsigned char> buffer);

	static SubmitAnswerRequest deserializeSubmitAnswerRequest(std::vector<unsigned char> buffer);
};

#endif // !JSONRESPONSEPACKETSERIALIZER
