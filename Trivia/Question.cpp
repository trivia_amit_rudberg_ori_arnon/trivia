#include "Question.h"
#include <algorithm>
#include <random>

Question::Question(std::string question, std::string correct_ans, std::string ans2, std::string ans3, std::string ans4)
{
    m_correctAnswerId = 0;
    m_question = question;
    m_possibleAnswers.push_back(correct_ans);
    m_possibleAnswers.push_back(ans2);
    m_possibleAnswers.push_back(ans3);
    m_possibleAnswers.push_back(ans4);

    /*auto rng = std::default_random_engine{};
    std::shuffle(std::begin(m_possibleAnswers), std::end(m_possibleAnswers), rng);*/

    std::random_shuffle(m_possibleAnswers.begin(), m_possibleAnswers.end());

    for (int i = 0; i < m_possibleAnswers.size(); i++)
    {
        if (m_possibleAnswers[i] == correct_ans)
        {
            m_correctAnswerId = i;
            break;
        }
    }
}

std::string Question::getQuestion()
{
    return m_question;
}

std::vector<std::string> Question::getPossibleAnswers()
{
    auto possibleAnswer = m_possibleAnswers;
    return m_possibleAnswers;
}

int Question::CorrectAnswerId()
{
    return m_correctAnswerId;
}

void Question::setQuestion(std::string question)
{
    m_question = question;
}

