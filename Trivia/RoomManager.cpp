#include "RoomManager.h"

void RoomManager::createRoom(LoggedUser& user , RoomData roomData)
{
	Room room;
	room.addUser(user);

	roomData.id = id_counter;

	room.setRoomData(roomData);
	m_rooms[id_counter] = room;
	id_counter++;
}

void RoomManager::deleteRoom(int ID)
{
	m_rooms.erase(ID);
}

unsigned int RoomManager::getIdCounter()
{
	return id_counter;
}

unsigned int RoomManager::getRoomState(int ID)
{
	return m_rooms[ID].getRoomData().isActive;
}

std::vector<RoomData>& RoomManager::getRooms()
{
	std::vector<RoomData>* rooms = new std::vector<RoomData>();
	for (auto it = m_rooms.begin(); it != m_rooms.end(); it++)
	{
		rooms->push_back(it->second.getRoomData());
	}
	return *rooms;
}

Room& RoomManager::getRoom(int ID)
{
	return m_rooms[ID];
}
