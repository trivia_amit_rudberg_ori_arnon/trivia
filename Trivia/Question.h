#pragma once
#include <string>
#include <vector>
class Question
{
public:
	Question(std::string question, std::string correct_ans,std::string ans2, std::string ans3, std::string ans4);
	Question() = default;
	std::string getQuestion();
	std::vector<std::string> getPossibleAnswers();
	int CorrectAnswerId();
	void setQuestion(std::string question);
private:
	std::string m_question;
	std::vector<std::string> m_possibleAnswers;
	unsigned int m_correctAnswerId;
};


