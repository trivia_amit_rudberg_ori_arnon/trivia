#ifndef LOGINREQUESTHANDLER_H
#define LOGINREQUESTHANDLER_H
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"

class RequestHandlerFactory;

class LoginRequestHandler : public IRequestHandler
{
public:
	LoginRequestHandler(RequestHandlerFactory& factory);
	~LoginRequestHandler();

	bool isRequestRelevant(RequestInfo& info) override;
	RequestResult handleRequest(RequestInfo& info) override;

private:
	RequestHandlerFactory& m_handleFactory;
};

#endif