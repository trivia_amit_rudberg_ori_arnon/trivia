#include "GameRequestHandler.h"

GameRequestHandler::GameRequestHandler(RequestHandlerFactory& factory, LoggedUser& user, GameManager& gm, Game* game) : m_game(game), m_user(user), m_handlerFactory(factory), m_gameManager(gm) {}

GameRequestHandler::~GameRequestHandler() {};

bool GameRequestHandler::isRequestRelevant(RequestInfo& info)
{
	return info.id == MessageCodes::GET_QUESTION_CODE ||
		info.id == MessageCodes::ANSWER_CODE ||
		info.id == MessageCodes::GAME_RESULTS_CODE ||
		info.id == MessageCodes::LEAVE_GAME_CODE;
}

RequestResult GameRequestHandler::handleRequest(RequestInfo& info)
{
	RequestResult result;

	if (!isRequestRelevant(info))
	{
		result.newHandler = nullptr;
		return result;
	}

	switch (info.id)
	{
	case MessageCodes::GET_QUESTION_CODE:
		result = getQuestion(info);
		break;
	case MessageCodes::ANSWER_CODE:
		result = submitAnswer(info);
		break;
	case MessageCodes::GAME_RESULTS_CODE:
		result = getGameResults(info);
		break;
	case MessageCodes::LEAVE_GAME_CODE:
		result = leaveGame(info);
		break;
	default:
		break;
	}


	return result;
}

RequestResult GameRequestHandler::getQuestion(RequestInfo& info)
{
	RequestResult result;
	GetQuestionResponse response;
	Question q = m_game->getQuestionForUser(m_user);
	//response.question = m_game.getQuestionForUser(m_user).getQuestion();
	response.question = q.getQuestion();
	int i = 0;
	for (std::string& answer : q.getPossibleAnswers())
	{
		response.answers.insert(std::pair<unsigned int, std::string>(i, answer));
		i++;
	}
	response.status = 0;

	result.response = JsonResponsePacketSerializer::SerializeResponse(response);
	result.newHandler = m_handlerFactory.createGameRequestHandler(m_user, m_game);
	return result;
}

RequestResult GameRequestHandler::submitAnswer(RequestInfo& info)
{
	RequestResult result;
	SubmitAnswerResponse response;
	SubmitAnswerRequest request = JsonResponsePacketDeserializer::deserializeSubmitAnswerRequest(info.buffer);
	
	response.correctAnswerId = m_game->getQuestionForUser(m_user).CorrectAnswerId();
	m_game->submitAnswer(m_user, request.answerId);

	response.status = 0;

	result.response = JsonResponsePacketSerializer::SerializeResponse(response);
	result.newHandler = m_handlerFactory.createGameRequestHandler(m_user, m_game);

	return result;
}

RequestResult GameRequestHandler::getGameResults(RequestInfo& info)
{
	RequestResult result;
	GetGameResultsResponse response;
	try
	{
		response.status = 0;

		m_handlerFactory.getRoomManager().deleteRoom(m_game->getGameId());
		
		for (std::pair<LoggedUser, GameData> pd : m_game->getPlayerMap())
		{
			
			PlayerResults pr;
			pr.avarageAnswerTime = pd.second.averangeAnswerTime;
			pr.correctAnswerCount = pd.second.correctAnswerCount;
			pr.username = pd.first.getUsername();
			pr.wrongAnswerCount = pd.second.wrongAnswerCount;
			response.results.push_back(pr);
		}

		result.newHandler = m_handlerFactory.createGameRequestHandler(m_user, m_game);
		result.response = JsonResponsePacketSerializer::SerializeResponse(response);
	}
	catch (const std::exception&)
	{
		response.status = 1;
		result.newHandler = nullptr;
		ErrorResponse er;
		er.message = "ending game failed";
		result.response = JsonResponsePacketSerializer::SerializeResponse(er);
	}
	
	return result;
}

RequestResult GameRequestHandler::leaveGame(RequestInfo& info)
{
	RequestResult result;
	LeaveGameResponse response;

	try
	{
		m_game->removePlayer(m_user);
		response.status = 0;

		if (m_game->getPlayerMap().size() == 0)
		{
			m_gameManager.deleteGame(m_game->getGameId());
		}
	}
	catch (const std::exception&)
	{
		response.status = 1;
	}

	result.response = JsonResponsePacketSerializer::SerializeResponse(response);
	result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);
	return result;
}