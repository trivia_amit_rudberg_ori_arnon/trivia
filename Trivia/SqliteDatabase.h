#pragma once
#include "IDatabase.h"
#include "sqlite3.h"
#include <vector>
#include "LoggedUser.h"
class SqliteDatabase : public IDatabase
{
public:
	SqliteDatabase();
	~SqliteDatabase();

	bool open()  override;
	bool close()  override;
	int doesUserExist(string User)  override;
	int doesPasswordMatch(string User, string Password)  override;
	int addNewUser(string User, string Password, string mail)  override;

	std::list<Question> getQuestions(int amount) override;

	double getPlayerAverageTime(std::string user) override;
	int getNumOfCorrectAnswers(std::string user) override;
	int getNumOfTotalAnswers(std::string user) override;
	int getNumOfPlayerGames(std::string user) override;

	std::vector<string> getHighScores() override;
	int getPlayerScore(std::string user) override;

	void submitGameStatistics(LoggedUser loggeduser, GameData gameData) override;

private:
	sqlite3* _db;
	int sendSQL(std::string statement);
	int recvSQL(std::string statement, int(*callback)(void*, int, char**, char**), void* data);
	static int callbackUser(void* data, int argc, char** argv, char** azColName);
	static int callbackQuestions(void* data, int argc, char** argv, char** azColName);
	static int callbackScores(void* data, int argc, char** argv, char** azColName);
	static int callbackStatistics(void* data, int argc, char** argv, char** azColName);
};

