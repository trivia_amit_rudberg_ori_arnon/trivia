#pragma once
#include <map>
#include "Room.h"
#include "RoomData.h"
#include "LoggedUser.h"

class RoomManager
{
public:
	void createRoom(LoggedUser& user, RoomData roomData);
	void deleteRoom(int ID);
	unsigned int getRoomState(int ID);
	std::vector<RoomData>& getRooms();
	Room& getRoom(int ID);
	unsigned int getIdCounter();
private:
	std::map< unsigned int , Room > m_rooms;
	unsigned int id_counter;
};
