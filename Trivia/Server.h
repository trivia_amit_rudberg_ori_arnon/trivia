#ifndef SERVER_H
#define SERVER_H
#include <iostream>
#include <thread>
#include "Communicator.h"
#include "LoginRequestHandler.h"
#include "IRequestHandler.h"
#include "IDatabase.h"
#include "RequestHandlerFactory.h"

class Server
{
public:
	Server();
	~Server();

	void run();

private:
	IDatabase* m_dataBase;

	RequestHandlerFactory m_requestHandlerFactory;

	Communicator m_communicator;
};

#endif