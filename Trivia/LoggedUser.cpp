#include "LoggedUser.h"

LoggedUser::LoggedUser(std::string username)
{
    m_username = username;
}

std::string LoggedUser::getUsername()
{
    return m_username;
}

void LoggedUser::setUserName(std::string name)
{
    m_username = name;
}

bool LoggedUser::operator<(const LoggedUser& other) const noexcept
{

    return this->m_username < other.m_username;
}
