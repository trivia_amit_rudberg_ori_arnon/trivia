#include "JsonResponsePacketSerializer.h"
#include "json.hpp"

using json = nlohmann::json;

std::vector<unsigned char> JsonResponsePacketSerializer::SerializeResponse(ErrorResponse response)
{
	std::string json = "{\"message\":\"" + response.message + "\"}";
	return SerializeJson(json, MessageCodes::ERROR_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::SerializeResponse(LoginResponse response)
{
	std::string json = "{\"status\": " + std::to_string(response.status) + "}";
	return SerializeJson(json, MessageCodes::LOGIN_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::SerializeResponse(SignUpResponse response)
{
	std::string json = "{\"status\": " + std::to_string(response.status) + "}";
	return SerializeJson(json, MessageCodes::SIGNUP_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::SerializeResponse(LogoutResponse response)
{
	std::string json = "{\"status\": " + std::to_string(response.status) + "}";
	
	return SerializeJson(json, MessageCodes::LOGOUT_CODE);
}
std::vector<unsigned char> JsonResponsePacketSerializer::SerializeResponse(GetRoomsResponse response)
{
	std::string json = "{\"status\": " + std::to_string(response.status) + ", \"rooms\": [";
	for (const RoomData& r : response.rooms)
	{
		json += "[" + std::to_string(r.id) + ", " + "\"" + r.name + "\"], ";
	}
	if (response.rooms.size() != 0)
	{
		json.pop_back();
		json.pop_back();
	}
	json += "] }";
	return SerializeJson(json, MessageCodes::GET_ROOMS_CODE);
}
std::vector<unsigned char> JsonResponsePacketSerializer::SerializeResponse(GetPlayersInRoomResponse response)
{
	std::string json = "{\"players\": [";
	for (const std::string& p : response.players)
	{
		json += "\"" + p + "\", ";
	}
	json.pop_back();
	json.pop_back();
	json += "] }";
	return SerializeJson(json, MessageCodes::PLAYERS_IN_ROOM_CODE);
}
std::vector<unsigned char> JsonResponsePacketSerializer::SerializeResponse(JoinRoomResponse response)
{
	std::string json = "{\"status\": " + std::to_string(response.status) + "}";
	return SerializeJson(json, MessageCodes::JOIN_ROOM_CODE);
}
std::vector<unsigned char> JsonResponsePacketSerializer::SerializeResponse(CreateRoomResponse response)
{
	std::string json = "{\"status\": " + std::to_string(response.status) + "}";
	return SerializeJson(json, MessageCodes::CREATE_ROOM_CODE);
}
std::vector<unsigned char> JsonResponsePacketSerializer::SerializeResponse(getHighScoreResponse response)
{
	std::string json = "{\"statistics\": [";
	for (const std::string& p : response.statistics)
	{
		json += p + ", ";
	}
	if (response.statistics.size() != 0)
	{
		json.pop_back();
		json.pop_back();
	}
	json += "] }";
	return SerializeJson(json, MessageCodes::HIGH_SCORE_CODE);
}
std::vector<unsigned char> JsonResponsePacketSerializer::SerializeResponse(getPersonalStatsResponse response)
{
	std::string json = "{\"statistics\": [";
	for (const std::string& p : response.statistics)
	{
		json += p + ", ";
	}
	if (response.statistics.size() != 0)
	{
		json.pop_back();
		json.pop_back();
	}
	json += "] }";
	return SerializeJson(json, MessageCodes::PERSONAL_STATS_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::SerializeResponse(CloseRoomResponse response)
{
	std::string json = "{\"status\": " + std::to_string(response.status) + "}";
	return SerializeJson(json, MessageCodes::CLOSE_ROOM_CODE);
}
std::vector<unsigned char> JsonResponsePacketSerializer::SerializeResponse(StartGameResponse response)
{
	std::string json = "{\"status\": " + std::to_string(response.status) + "}";
	return SerializeJson(json, MessageCodes::START_GAME_CODE);
}
std::vector<unsigned char> JsonResponsePacketSerializer::SerializeResponse(GetRoomStateResponse response)
{
	std::string json = "{\"id\": " + std::to_string(response.id) + ", \"status\": " + std::to_string(response.status) +
		",\"hasGameBegun\": " + ((response.hasGameBegun) ? "true" : "false") +
		", \"players\": [";
	for (std::string& player : response.players)
	{
		json += "\"" + player + "\", ";
	}

	if (response.players.size() > 0)
	{
		json.pop_back();
		json.pop_back();
	}
	json += "], \"questionCount\": " + std::to_string(response.questionCount) +
		", \"answerTimeout\": " + std::to_string(response.answerTimeout) + "}";
	return SerializeJson(json, MessageCodes::ROOM_STATE_CODE);
}
std::vector<unsigned char> JsonResponsePacketSerializer::SerializeResponse(LeaveRoomResponse response)
{
	std::string json = "{\"status\": " + std::to_string(response.status) + "}";
	return SerializeJson(json, MessageCodes::LEAVE_ROOM_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::SerializeResponse(GetGameResultsResponse response)
{
	std::string json = "{\"status\": " + std::to_string(response.status) + ", \"results\": [";
	for (PlayerResults& pr : response.results)
	{
		json += "[\"" + pr.username + "\", " + std::to_string(pr.avarageAnswerTime) + ", " +
			std::to_string(pr.correctAnswerCount) + ", " +
			std::to_string(pr.wrongAnswerCount) + "], ";
	}
	if (response.results.size() > 0)
	{
		json.pop_back();
		json.pop_back();
	}
	json += "]}";
	return SerializeJson(json, MessageCodes::GAME_RESULTS_CODE);
}
std::vector<unsigned char> JsonResponsePacketSerializer::SerializeResponse(SubmitAnswerResponse response)
{
	std::string json = "{\"status\": " + std::to_string(response.status) + ", \"correctAnswerId\":" + std::to_string(response.correctAnswerId) + "}";
	return SerializeJson(json, MessageCodes::ANSWER_CODE);
}
std::vector<unsigned char> JsonResponsePacketSerializer::SerializeResponse(GetQuestionResponse response)
{

	std::string json = "{\"status\": " + std::to_string(response.status) + ", \"question\": \"" + response.question + "\", \"answers\": [";
	for (auto i = response.answers.begin(); i != response.answers.end(); i++)
	{
		json += "[" + std::to_string(i->first) + ", \"" + i->second + "\"], ";
	}
	if (response.answers.size() > 0)
	{
		json.pop_back();
		json.pop_back();
	}
	json += "]}";
	return SerializeJson(json, MessageCodes::GET_QUESTION_CODE);
}
std::vector<unsigned char> JsonResponsePacketSerializer::SerializeResponse(LeaveGameResponse response)
{
	std::string json = "{\"status\": " + std::to_string(response.status) + "}";
	return SerializeJson(json, MessageCodes::LEAVE_GAME_CODE);
}

std::vector<unsigned char> JsonResponsePacketSerializer::SerializeJson(std::string json, int messageCode)
{
	std::vector<unsigned char> result;

	std::string length = std::to_string(json.length());
	std::string paddedLength = std::string(4 - ((4 > length.size()) ? length.size() : 4), '0').append(length);
	std::string code = std::to_string(messageCode);

	std::copy(code.begin(), code.end(), std::back_inserter(result));
	std::copy(paddedLength.begin(), paddedLength.end(), std::back_inserter(result));
	std::copy(json.begin(), json.end(), std::back_inserter(result));

	return result;
}