#include "Server.h"
#include "WSAInitializer.h"
#include "SqliteDatabase.h"

Server::Server() : m_communicator(m_requestHandlerFactory), m_requestHandlerFactory(m_dataBase), m_dataBase(new SqliteDatabase())
{
	WSAInitializer wsaInit;
}

Server::~Server() {}

void Server::run()
{
	m_dataBase->open();
	std::thread t_connector(&Communicator::startHandleRequests, m_communicator);
	t_connector.detach();

	std::string command;

	do
	{
		std::cin >> command;

	} while (command != "EXIT");


}