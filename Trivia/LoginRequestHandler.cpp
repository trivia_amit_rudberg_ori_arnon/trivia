#include "LoginRequestHandler.h"
#include "IRequestHandler.h"
#include "MenuRequestHandler.h"
#include "MessageCodes.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "RequestHandlerFactory.h"
#include "RequestStructs.h"
#include "LoggedUser.h"

LoginRequestHandler::LoginRequestHandler(RequestHandlerFactory& factory) : m_handleFactory(factory) {};

bool LoginRequestHandler::isRequestRelevant(RequestInfo& info)
{
	return info.id == MessageCodes::LOGIN_CODE || info.id == MessageCodes::SIGNUP_CODE;
}

RequestResult LoginRequestHandler::handleRequest(RequestInfo& info)
{
	LoggedUser loggedUser("");
	RequestResult result;
	bool error = false;

	if (!isRequestRelevant(info))
	{
		result.newHandler = nullptr;
	}
	else if (info.id == MessageCodes::LOGIN_CODE)
	{
		LoginRequest request = JsonResponsePacketDeserializer::deserializeLoginRequest(info.buffer);
		std::cout << "login details: " << request.username << "  " << request.password << std::endl;
		loggedUser.setUserName(request.username);
		LoginResponse response;
		response.status = m_handleFactory.getLoginManager().login(request.username, request.password);
		error = response.status == 1;
		result.response = JsonResponsePacketSerializer::SerializeResponse(response);
	}
	else if (info.id == MessageCodes::SIGNUP_CODE)
	{
		SignUpRequest request = JsonResponsePacketDeserializer::deserializeSignUpRequest(info.buffer);
		std::cout << "signup details: " << request.username << "  " << request.password << "  " << request.email << std::endl;
		loggedUser.setUserName(request.username);
		SignUpResponse response;
		response.status = m_handleFactory.getLoginManager().signup(request.username, request.password, request.email);
		error = response.status == 1;
		result.response = JsonResponsePacketSerializer::SerializeResponse(response);
	}

	if (error || result.response.size() == 0 || (result.response[0] == '1' && result.response[1] == '0' && result.response[2] == '0'))
	{
		result.newHandler = m_handleFactory.createLoginRequestHandler();
	}
	else
	{
		result.newHandler = m_handleFactory.createMenuRequestHandler(loggedUser);
	}
	
	return result;
}