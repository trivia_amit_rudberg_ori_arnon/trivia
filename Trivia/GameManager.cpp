#include "GameManager.h"

GameManager::GameManager(IDatabase* db) : m_database(db)
{
}

Game& GameManager::createGame(Room& room)
{
    Game* game = new Game(room.getRoomData().id, room.getRoomData().numOfQuestionInGame, m_database, room.getAllUsers());
	m_games.push_back(game);
	return *game;
}

Game* GameManager::getGame(unsigned int gameId)
{
	for (auto i = m_games.begin(); i != m_games.end(); i++)
	{
		if ((*i)->getGameId() == gameId)
		{
			return *i;
		}
	}
	std::cout << "cant find game" << std::endl;
	return *(m_games.begin());
}

void GameManager::deleteGame(unsigned int gameId)
{
	for (auto i = m_games.begin(); i != m_games.end(); i++)
	{
		if ((*i)->getGameId() == gameId)
		{
			m_games.erase(i);
			break;
		}
	}
}
