#include "MenuRequestHandler.h"
#include "MessageCodes.h"
#include "RequestStructs.h"
#include "ResponseStructs.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "LoginManager.h"
#include "StatisticsManager.h"

MenuRequestHandler::MenuRequestHandler(RequestHandlerFactory& factory, LoggedUser& user) : m_handlerFactory(factory), m_user(user) {};

MenuRequestHandler::~MenuRequestHandler() {};

bool MenuRequestHandler::isRequestRelevant(RequestInfo& info)
{
	return 
		info.id == MessageCodes::LOGOUT_CODE ||
		info.id == MessageCodes::CREATE_ROOM_CODE ||
		info.id == MessageCodes::GET_ROOMS_CODE ||
		info.id == MessageCodes::HIGH_SCORE_CODE ||
		info.id == MessageCodes::JOIN_ROOM_CODE ||
		info.id == MessageCodes::PERSONAL_STATS_CODE ||
		info.id == MessageCodes::PLAYERS_IN_ROOM_CODE;
}

RequestResult MenuRequestHandler::handleRequest(RequestInfo& info)
{
	RequestResult result;

	if (!isRequestRelevant(info))
	{
		result.newHandler = nullptr;
		return result;
	}

	switch (info.id)
	{
	case MessageCodes::CREATE_ROOM_CODE :
		result = createRoom(info);
		break;
	case MessageCodes::GET_ROOMS_CODE :
		result = getRooms(info);
		break;
	case MessageCodes::HIGH_SCORE_CODE :
		result = getHighScore(info);
		break;
	case MessageCodes::JOIN_ROOM_CODE :
		result = joinRoom(info);
		break;
	case MessageCodes::LOGOUT_CODE :
		result = signOut(info);
		break;
	case MessageCodes::PERSONAL_STATS_CODE :
		result = getPersonalStats(info);
		break;
	case MessageCodes::PLAYERS_IN_ROOM_CODE :
		result = getPlayersInRoom(info);
		break;
	default:
		break;
	}

	return result;
}

RequestResult MenuRequestHandler::signOut(RequestInfo& info)
{
	RequestResult result;
	LogoutResponse response;
	m_handlerFactory.getLoginManager().logout(m_user.getUsername());
	for (RoomData& rd : m_handlerFactory.getRoomManager().getRooms())
	{
		m_handlerFactory.getRoomManager().getRoom(rd.id).removeUser(m_user);
	}
	response.status = 0;
	result.response = JsonResponsePacketSerializer::SerializeResponse(response);
	result.newHandler = m_handlerFactory.createLoginRequestHandler();
	return result;
}
RequestResult MenuRequestHandler::getRooms(RequestInfo& info)
{
	RequestResult result;
	GetRoomsResponse response;
	for (RoomData& rd : m_handlerFactory.getRoomManager().getRooms())
	{
		if (!rd.isActive)
		{
			response.rooms.insert(response.rooms.end(), rd);
		}
	}
	//response.rooms = m_handlerFactory.getRoomManager().getRooms();
	response.status = response.rooms.size() > 0 ? 0 : 1;
	result.response = JsonResponsePacketSerializer::SerializeResponse(response);
	result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);
	return result;
}
RequestResult MenuRequestHandler::getPlayersInRoom(RequestInfo& info)
{
	RequestResult result;
	GetPlayersInRoomResponse response;
	//response.players = m_handlerFactory.getRoomManager().getRoom(JsonResponsePacketDeserializer::deserializeGetPlayersRequest(info.buffer).roomId).getAllUsers();
	for (auto i : m_handlerFactory.getRoomManager().getRoom(JsonResponsePacketDeserializer::deserializeGetPlayersRequest(info.buffer).roomId).getAllUsers())
	{
		response.players.push_back(i.getUsername());
	}
	result.response = JsonResponsePacketSerializer::SerializeResponse(response);
	result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);
	return result;
}
RequestResult MenuRequestHandler::getPersonalStats(RequestInfo& info)
{
	RequestResult result;
	getPersonalStatsResponse response;
	response.statistics = m_handlerFactory.getStatisticsManager().getUserStatistics(m_user.getUsername());
	response.status = (response.statistics.size() > 0) ? 0 : 1;
	result.response = JsonResponsePacketSerializer::SerializeResponse(response);
	result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);
	return result;
}
RequestResult MenuRequestHandler::getHighScore(RequestInfo& info)
{
	RequestResult result;
	getHighScoreResponse response;
	response.statistics = m_handlerFactory.getStatisticsManager().getHighSore();
	result.response = JsonResponsePacketSerializer::SerializeResponse(response);
	result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);
	return result;
}
RequestResult MenuRequestHandler::joinRoom(RequestInfo& info)
{
	RequestResult result;
	JoinRoomResponse response;
	response.status = 0;
	for (RoomData& rd : m_handlerFactory.getRoomManager().getRooms())
	{
		for( LoggedUser& user : m_handlerFactory.getRoomManager().getRoom(rd.id).getAllUsers())
		{
			if (user.getUsername() == m_user.getUsername())
			{
				response.status = 1;
				break;
			}
		}
	}
	if (response.status == 0)
	{
		Room& room = m_handlerFactory.getRoomManager().getRoom(JsonResponsePacketDeserializer::deserializeJoinRoomRequest(info.buffer).roomId);
		room.addUser(m_user);
		result.newHandler = m_handlerFactory.createRoomMemberRequestHandler(m_user, room);
	}
	else
	{
		result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);
	}
	result.response = JsonResponsePacketSerializer::SerializeResponse(response);
	return result;
}
RequestResult MenuRequestHandler::createRoom(RequestInfo& info)
{
	RequestResult result;
	CreateRoomResponse response = CreateRoomResponse();
	response.status = 0;
	CreateRoomRequest request = JsonResponsePacketDeserializer::deserializeCreateRoomRequest(info.buffer);
	for (RoomData& rd : m_handlerFactory.getRoomManager().getRooms())
	{
		if (rd.name == request.roomName)
		{
			response.status = 1;
		}
	}
	if (response.status == 0)
	{
		RoomData rd;
		rd.id = m_handlerFactory.getRoomManager().getIdCounter();
		rd.isActive = false;
		rd.maxPlayers = request.maxUsers;
		rd.name = request.roomName;
		rd.numOfQuestionInGame = request.questionCount;
		rd.timePerQuestion = request.answerTimeout;
		m_handlerFactory.getRoomManager().createRoom(m_user, rd);
		result.newHandler = m_handlerFactory.createRoomAdminRequestHandler(m_user, m_handlerFactory.getRoomManager().getRoom(rd.id));
	}
	else
	{
		result.newHandler = m_handlerFactory.createMenuRequestHandler(m_user);
	}
	
	result.response = JsonResponsePacketSerializer::SerializeResponse(response);
	
	return result;
}