#include "LoginManager.h"
#include <iterator>
#include <algorithm>
#define FALSE 1
#define TRUE 0
#define MAX_LEN 25
LoginManager::LoginManager(IDatabase* db) : m_database(db) {};

int LoginManager::signup(string User, string Password, string mail)
{
	if (isInputProper(User) && isInputProper(Password) && isInputProper(mail))
	{
		return m_database->addNewUser(User, Password, mail);
	}
	return FALSE;
	//return 1; //return 0 if error - change in database
}

int LoginManager::login(string User, string Password)
{
	if (!(isInputProper(User) && isInputProper(Password)))
	{
		return FALSE;
	}
	if (isUserLogin(User))
	{
		std::cout << "ERROR : user login\n";
		return FALSE;
	}
	if (m_database->doesPasswordMatch(User, Password) == TRUE)
	{
		m_loggedUser.push_back(LoggedUser(User));
	}
	else
	{
		std::cout << "user don't exist\n";
		return FALSE;
	}
	return TRUE;
	//return 0 if error, 1 if works - change in database
}

int LoginManager::logout(string User)
{
	if (!isInputProper(User))
	{
		return FALSE;
	}
	for (auto it = m_loggedUser.begin(); it != m_loggedUser.end(); it++)
	{
		if (it->getUsername() == User)
		{
			m_loggedUser.erase(it);
			return TRUE;
		}
	}
	std::cout << "user don't exist";
	return FALSE;

	//return 0 if error, 1 if works - change in database
}

bool LoginManager::isInputProper(string str)
{
	for (int i = 0; i < str.length(); i++)
	{
		if ( str[i] == '\'' or str[i] == '"')
		{
			std::cout << "Error the input include ' or \" \n";
			return false;
		}
	}
	
	if (str.length() > MAX_LEN)
	{
		std::cout << "Error the input to long\n";
		return false;
	}
	return true;
}

bool LoginManager::isUserLogin(string User)
{
	for (auto it = m_loggedUser.begin(); it != m_loggedUser.end(); it++)
	{
		if (it->getUsername() == User)
		{
			return true;
		}
	}
	return false;
}
