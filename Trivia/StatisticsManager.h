#pragma once
#include "IDatabase.h"
#include <vector>
#include <string>

class StatisticsManager
{
public:
	StatisticsManager(IDatabase* db) : m_database(db) {}

	std::vector<std::string> getHighSore();
	std::vector<std::string> getUserStatistics(std::string username);
private:
	IDatabase* m_database;
};

