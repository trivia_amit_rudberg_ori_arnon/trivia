#pragma once
#include <string>

class LoggedUser
{
public:
	LoggedUser(std::string username);
	std::string getUsername();
	void setUserName(std::string name);
	bool operator<(const LoggedUser& other) const noexcept;
	
private:
	std::string m_username;
};

