#include "JsonRequestPacketDeserializer.h"
#include "json.hpp"
#include <iostream>

using json = nlohmann::json;

LoginRequest JsonResponsePacketDeserializer::deserializeLoginRequest(std::vector<unsigned char> buffer)
{
	LoginRequest result;
	std::vector<unsigned char> content(buffer.begin() + 4 + MESSAGE_CODE_LENGTH, buffer.end());
	json data = json::parse(content);

	result.password = data["password"];
	result.username = data["username"];

	return result;
}

SignUpRequest JsonResponsePacketDeserializer::deserializeSignUpRequest(std::vector<unsigned char> buffer)
{
	SignUpRequest result;
	std::vector<unsigned char> content(buffer.begin() + 4 + MESSAGE_CODE_LENGTH, buffer.end());
	json data = json::parse(content);

	result.password = data["password"];
	result.username = data["username"];
	result.email = data["email"];

	return result;
}

GetPlayersInRoomRequest JsonResponsePacketDeserializer::deserializeGetPlayersRequest(std::vector<unsigned char> buffer)
{
	GetPlayersInRoomRequest result;
	std::vector<unsigned char> content(buffer.begin() + 4 + MESSAGE_CODE_LENGTH, buffer.end());
	json data = json::parse(content);

	result.roomId = data["roomId"];
	
	return result;
}

JoinRoomRequest JsonResponsePacketDeserializer::deserializeJoinRoomRequest(std::vector<unsigned char> buffer)
{
	JoinRoomRequest result;
	std::vector<unsigned char> content(buffer.begin() + 4 + MESSAGE_CODE_LENGTH, buffer.end());
	json data = json::parse(content);

	result.roomId = data["roomId"];

	return result;
}

CreateRoomRequest JsonResponsePacketDeserializer::deserializeCreateRoomRequest(std::vector<unsigned char> buffer)
{
	CreateRoomRequest result;
	std::vector<unsigned char> content(buffer.begin() + 4 + MESSAGE_CODE_LENGTH, buffer.end());
	json data = json::parse(content);

	result.answerTimeout = data["answerTimeout"];
	result.maxUsers = data["MaxUser"];
	result.questionCount = data["questionCount"];
	result.roomName = data["roomName"];

	return result;
}

SubmitAnswerRequest JsonResponsePacketDeserializer::deserializeSubmitAnswerRequest(std::vector<unsigned char> buffer)
{
	SubmitAnswerRequest result;
	std::vector<unsigned char> content(buffer.begin() + 4 + MESSAGE_CODE_LENGTH, buffer.end());
	json data = json::parse(content);

	result.answerId = data["answerid"];

	return result;
}