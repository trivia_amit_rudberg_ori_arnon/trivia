#pragma once
#include "Question.h"
struct GameData
{
	Question currentQuestion;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	unsigned int averangeAnswerTime;
	unsigned int currAnswer;


	GameData()
	{
		correctAnswerCount = 0;
		wrongAnswerCount = 0;
		averangeAnswerTime = 0;
		currAnswer = 0;
	}
};

