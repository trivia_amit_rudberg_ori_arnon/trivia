#ifndef ROOMMEMBERREQUESTHANDLER_H
#define ROOMMEMBERREQUESTHANDLER_H
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "LoggedUser.h"

class RequestHandlerFactory;

class RoomMemberRequestHandler : public IRequestHandler
{
public:
	RoomMemberRequestHandler(RequestHandlerFactory& factory, LoggedUser& user, Room& room);
	~RoomMemberRequestHandler();

	bool isRequestRelevant(RequestInfo& info) override;
	RequestResult handleRequest(RequestInfo& info) override;
private:
	LoggedUser& m_user;
	Room& m_room;
	RequestHandlerFactory& m_handlerFactory;

	RequestResult leaveRoom(RequestInfo info);
	RequestResult getRoomState(RequestInfo info);
};

#endif