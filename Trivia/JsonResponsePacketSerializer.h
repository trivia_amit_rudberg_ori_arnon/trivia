#ifndef JSONRESPONSEPACKETSERIALIZER_H
#define JSONRESPONSEPACKETSERIALIZER_H
#include "ResponseStructs.h"
#include "MessageCodes.h"
#include <vector>
#include <algorithm>
#include <iterator>
#include <iomanip>
#include <iostream>
#include <string>

static class JsonResponsePacketSerializer
{
public:
	static std::vector<unsigned char> SerializeResponse(ErrorResponse response);
	static std::vector<unsigned char> SerializeResponse(LoginResponse response);
	static std::vector<unsigned char> SerializeResponse(SignUpResponse response);

	static std::vector<unsigned char> SerializeResponse(LogoutResponse response);
	static std::vector<unsigned char> SerializeResponse(GetRoomsResponse response);
	static std::vector<unsigned char> SerializeResponse(GetPlayersInRoomResponse response);
	static std::vector<unsigned char> SerializeResponse(JoinRoomResponse response);
	static std::vector<unsigned char> SerializeResponse(CreateRoomResponse response);
	static std::vector<unsigned char> SerializeResponse(getHighScoreResponse response);
	static std::vector<unsigned char> SerializeResponse(getPersonalStatsResponse response);

	static std::vector<unsigned char> SerializeResponse(CloseRoomResponse response);
	static std::vector<unsigned char> SerializeResponse(StartGameResponse response);
	static std::vector<unsigned char> SerializeResponse(GetRoomStateResponse response);
	static std::vector<unsigned char> SerializeResponse(LeaveRoomResponse response);

	static std::vector<unsigned char> SerializeResponse(GetGameResultsResponse response);
	static std::vector<unsigned char> SerializeResponse(SubmitAnswerResponse response);
	static std::vector<unsigned char> SerializeResponse(GetQuestionResponse response);
	static std::vector<unsigned char> SerializeResponse(LeaveGameResponse response);
private:
	static std::vector<unsigned char> SerializeJson(std::string json, int messageCode);
};

#endif // !JSONRESPONSEPACKETSERIALIZER
