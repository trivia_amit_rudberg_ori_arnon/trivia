#include "SqliteDatabase.h"
#include "sqliteDatabaseStructs.h"
#include <iostream>
#include <io.h>
#define FALSE 1
#define TRUE 0

#define DB_FILE_NAME "TriviaDB.db"

using std::cout;
using std::endl;


SqliteDatabase::SqliteDatabase()
{
    open();
}

SqliteDatabase::~SqliteDatabase()
{
    close();
}

bool SqliteDatabase::open() 
{
    const char* questions = R"(
        INSERT INTO t_questions (question, correct_ans, ans2, ans3, ans4)
        VALUES
        ('Which of these is impossible to do?', 'Watch the Olympics at February 30th', 'Watch the world cup at June 10th', 'Watch the summer games at July 20th', 'Watch the Eurovision at May 1st'),
        ('How many sides does a circle has?','2','0', '1', '3'),
        ('What starts with an ''e'', ends with an ''e'', but only has one letter?', 'Envelope', 'E', 'Ee', 'Eye'),
        ('What has four eyes but cant see?', 'Mississippi', 'Cerberus', 'Infinity', 'Inquilinic'),
        ('What is the 8th letter of the alphabet?', 'A', 'H',  'G', 'T'),
        ('What is the answer to life, the universe, and everything?', '42', 'Friendship', '17', 'Pi'),
        ('What do you call a wingless fly?', 'A walk', 'Bobby', 'A flap', 'A plum'),
        ('how many holes are in a polo', '4', '1', '2', '3'),
        ('Who is the best Amit?', 'Rudberg', 'Turgeman', 'Vigder', 'None'),
        ('STOP!',  'Hammer time','o.k.',  'Stop timer', 'BEHIND YOU!'),
        ('Can a match box?','No, but a tin can', 'No', 'Yes',  'Yes, one beat up dan'),
        ('how many tickles do you need to get an octopus to laugh?', '10', '8',  '12', '14'),
        ('What is the answer to this question: 1+1=', 'What', 'Window', '2', '11'),
        ('Whats heavier?', 'A kilogram of steel', 'A kilogram of feathers', 'I dont get it', 'They are both a kilogram'),
        ('if 10*2 is 20, that means 11*2 is', '20', '22', '18', '24'),
        ('what word is always spelled wrong?', 'Wrong', 'Wednesday', 'Environment', 'Word'),
        ('assassins has 5 s and 2 a. can you spell that without using s or a?', 'Tht', 'In', 'Niss', 'Ass'),
        ('you just passed the person in 2nd place. What place are you in?', '2nd', '1st', '3rd', '4th'),
        ('What letter has the most water in it?', 'C', 'A', 'H', 'Q'),
        ('What kind of cheese is made backwards?', 'Edam', 'Swiss', 'Blue', 'Milbenkase'),
        ('its 2023. Who was born in the 90s?', 'My great Grandpa whos 125 years old', 'My grandma whos 88 years old', 'My dad whos 38 years old', 'Me, im 14 years old');
    )";
    string dbFileName = DB_FILE_NAME;
    int file_exist = _access(dbFileName.c_str(), 0);
    int res = sqlite3_open(dbFileName.c_str(), &_db);
    if (res != SQLITE_OK) 
    {
        _db = nullptr;
        cout << "Failed to open DB" << endl;
        return false;
    }
    if (file_exist != 0) {
        return sendSQL("CREATE TABLE  IF NOT EXISTS t_users (username TEXT PRIMARY KEY NOT NULL ,password TEXT NOT NULL,email TEXT NOT NULL)")
            or sendSQL("CREATE TABLE  IF NOT EXISTS t_questions ( question_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,question TEXT UNIQUE NOT NULL,correct_ans TEXT NOT NULL,ans2 TEXT NOT NULL,ans3 TEXT NOT NULL,ans4 TEXT NOT NULL)")
            or sendSQL(questions)
            or sendSQL("CREATE TABLE  IF NOT EXISTS t_statistics (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,username TEXT NOT NULL,correct_answer INTEGER NOT NULL,wrong_answer INTEGER NOT NULL,answer_time INTEGER NOT NULL)");
    }
    
}

bool SqliteDatabase::close() 
{
    sqlite3_close(_db);
    _db = nullptr;
    return true;
}

int SqliteDatabase::doesUserExist(string User) 
{
    t_users user;
    if (recvSQL("select * from t_users WHERE username = '" + User + "';", &SqliteDatabase::callbackUser, &user))
    {
        return FALSE;
    }
    if (user.username.empty() || user.password.empty() || user.email.empty())
    {
        return FALSE;
    }
    return TRUE;
}

int SqliteDatabase::doesPasswordMatch(string User, string Password)
{
    t_users user;
    if (recvSQL("select * from t_users WHERE username = '" + User + "' AND password = '" + Password + "';", &SqliteDatabase::callbackUser, &user))
    {
        return FALSE;
    }
    if (user.username.empty() || user.password.empty() || user.email.empty())
    {
        return FALSE;
    }
    return TRUE;
}

int SqliteDatabase::addNewUser(string User, string Password, string mail)
{
    if (doesUserExist(User) == TRUE)
    {
        std::cout << "The username is taken\n";
        return FALSE;
    }
    if (sendSQL("INSERT INTO t_users (username, password, email) VALUES ('" + User + "', '" + Password + "', '" + mail + "'); "))
    {
        return FALSE;
    }
    return TRUE;
}

std::list<Question> SqliteDatabase::getQuestions(int amount)
{
    std::list<Question> questions;
    recvSQL("SELECT question,correct_ans,ans2,ans3,ans4 FROM t_questions ORDER BY RANDOM() LIMIT " + std::to_string(amount),&SqliteDatabase::callbackQuestions, &questions);
    return questions;
}

std::vector<string> SqliteDatabase::getHighScores()
{
    std::vector<string> scores;
    recvSQL("SELECT username, sum(correct_answer) AS score FROM t_statistics  GROUP BY username ORDER BY score DESC LIMIT 4;", &SqliteDatabase::callbackScores,&scores);
    return scores;
}

int SqliteDatabase::getNumOfCorrectAnswers(std::string user)
{
    double statistics = 0;
    recvSQL("SELECT sum(correct_answer) AS statistics FROM t_statistics WHERE username = '"+user+"' GROUP BY username", &SqliteDatabase::callbackStatistics,&statistics);
    return (int)statistics;
}

int SqliteDatabase::getNumOfPlayerGames(std::string user)
{
    double statistics = 0;
    recvSQL("SELECT count(*) AS statistics FROM t_statistics WHERE username = '"+user+"'", &SqliteDatabase::callbackStatistics, &statistics);
    return (int)statistics;
}

int SqliteDatabase::getNumOfTotalAnswers(std::string user)
{
    double statistics = 0;
    recvSQL("SELECT coalesce(sum(correct_answer + wrong_answer), 0) AS statistics FROM t_statistics WHERE username = '"+user+"'; ", &SqliteDatabase::callbackStatistics, &statistics);
    return (int)statistics;
}

double SqliteDatabase::getPlayerAverageTime(std::string user)
{
    double statistics = 0;
    recvSQL("SELECT avg(answer_time) AS statistics FROM t_statistics WHERE  username  = '"+user+"'  GROUP BY username", &SqliteDatabase::callbackStatistics, &statistics);
    return statistics;
}

int SqliteDatabase::getPlayerScore(std::string user)
{
    return 0;
}

void SqliteDatabase::submitGameStatistics(LoggedUser loggeduser, GameData gameData)
{
    sendSQL("INSERT INTO t_statistics (username,correct_answer,wrong_answer,answer_time) VALUES ('"+loggeduser.getUsername() + "',"+std::to_string(gameData.correctAnswerCount) + "," +std::to_string(gameData.wrongAnswerCount)+","+ std::to_string(gameData.averangeAnswerTime) +")");
}

int SqliteDatabase::sendSQL(std::string statement)
{
    char* errMessage = nullptr;
    int result = 0;
    if ((result = sqlite3_exec(_db, statement.c_str(), nullptr, nullptr, &errMessage)) != SQLITE_OK)
    {
        std::cout << "query failed: " << errMessage << std::endl;
    }

    return result;
}

int SqliteDatabase::recvSQL(std::string statement, int(*callback)(void*, int, char**, char**), void* data)
{
    char* errMessage = nullptr;
    int result = 0;
    if ((result = sqlite3_exec(_db, statement.c_str(), callback, data, &errMessage)) != SQLITE_OK)
    {
        std::cout << "query failed: " << errMessage << std::endl;
    }

    return result;
}

int SqliteDatabase::callbackUser(void* data, int argc, char** argv, char** azColName)
{
     t_users* user = static_cast< t_users*>(data);
    for (int i = 0; i < argc; i++)
    {
        if (string(azColName[i]) == "username") {
            user->username = argv[i];
        }
        else if (string(azColName[i]) == "password") {
            user->password = argv[i];
        }
        else if (string(azColName[i]) == "email") {
            user->email = argv[i];
        }
        
    }
    return 0;
}

int SqliteDatabase::callbackQuestions(void* data, int argc, char** argv, char** azColName)
{
    std::list<Question>* questionList = static_cast<std::list<Question>*>(data);
    std::string question, correct_ans, ans2, ans3, ans4;
    for (int i = 0; i < argc; i++)
    {
        if (string(azColName[i]) == "question") {
            question=argv[i];
        }
        else if (string(azColName[i]) == "correct_ans") {
            correct_ans=argv[i];
        }
        else if (string(azColName[i]) == "ans2") {
            ans2 = argv[i];
        }
        else if (string(azColName[i]) == "ans3") {
            ans3 = argv[i];
        }
        else if (string(azColName[i]) == "ans4") {
            ans4 = argv[i];
        }

    }
    Question Question(question,correct_ans,ans2,ans3,ans4);
    questionList->push_back(Question);
    return 0;
}

int SqliteDatabase::callbackScores(void* data, int argc, char** argv, char** azColName)
{
    std::string username, score;
    std::vector<string>* scores = static_cast<std::vector<string>*>(data);
    for (int i = 0; i < argc; i++)
    {
        if (string(azColName[i]) == "username") {
            username = std::string(argv[i]);
        }
        else if (string(azColName[i]) == "score") {
            score=argv[i];
        }
        

    }
    scores->push_back("\"" + username + " - " + score + "\"");
    return 0;
}

int SqliteDatabase::callbackStatistics(void* data, int argc, char** argv, char** azColName)
{
    double* statistics = static_cast <double*>(data);
    if (string(azColName[0]) == "statistics")
    {
        *statistics = (argv == NULL) ? 0 : std::stod(argv[0]);
    }
    return 0;
}



