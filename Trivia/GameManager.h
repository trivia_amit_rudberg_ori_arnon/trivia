#pragma once
#include "IDatabase.h"
#include "Game.h"
#include <vector>
#include "Room.h"
#include <iterator>
#include <iostream>

class GameManager
{
public:
	GameManager(IDatabase* db);
	Game& createGame(Room& room);
	Game* getGame(unsigned int gameId);
	void deleteGame(unsigned int gameId);
private:
	IDatabase* m_database;
	std::vector <Game*> m_games;
};
