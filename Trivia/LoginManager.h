#pragma once
#include "IDatabase.h"
#include "LoggedUser.h"
#include <vector>
#include <string>
#include <iostream>

using std::string;

class LoginManager
{
public:
	LoginManager(IDatabase* db);

	int signup(string User, string Password, string mail);
	int login(string User, string Password);
	int logout(string User);
private:
	IDatabase* m_database;
	std::vector<LoggedUser> m_loggedUser;
	bool isInputProper(string str);
	bool isUserLogin(string User);
};

