#include "RequestHandlerFactory.h"

RequestHandlerFactory::RequestHandlerFactory(IDatabase* db) : m_loginManager(db), m_dataBase(db), m_statisticsManager(db), m_gameManager(db) {};

LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
	return new LoginRequestHandler(*this);
}

MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(LoggedUser& loggedUser)
{
	return new MenuRequestHandler(*this, loggedUser);
}

RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(LoggedUser& loggedUser, Room& room)
{
	return new RoomAdminRequestHandler(*this, loggedUser, room);
}
RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(LoggedUser& loggedUser, Room& room)
{
	return new RoomMemberRequestHandler(*this, loggedUser, room);
}

GameRequestHandler* RequestHandlerFactory::createGameRequestHandler(LoggedUser& loggedUser, Game* game)
{
	return new GameRequestHandler(*this, loggedUser, m_gameManager, game);
}

LoginManager& RequestHandlerFactory::getLoginManager()
{
	return m_loginManager;

}

RoomManager& RequestHandlerFactory::getRoomManager()
{
	return m_roomManager;
}

StatisticsManager& RequestHandlerFactory::getStatisticsManager()
{
	return m_statisticsManager;
}
GameManager& RequestHandlerFactory::getGameManager()
{
	return m_gameManager;
}
