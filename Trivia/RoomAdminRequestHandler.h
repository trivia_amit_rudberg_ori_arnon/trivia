#ifndef ROOMADMINREQUESTHANDLER_H
#define ROOMADMINREQUESTHANDLER_H
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "LoggedUser.h"

class RequestHandlerFactory;

class RoomAdminRequestHandler : public IRequestHandler
{
public:
	RoomAdminRequestHandler(RequestHandlerFactory& factory, LoggedUser& user, Room& room);
	~RoomAdminRequestHandler();

	bool isRequestRelevant(RequestInfo& info) override;
	RequestResult handleRequest(RequestInfo& info) override;
private:
	LoggedUser& m_user;
	Room& m_room;
	RequestHandlerFactory& m_handlerFactory;

	RequestResult closeRoom(RequestInfo info);
	RequestResult startGame(RequestInfo info);
	RequestResult getRoomState(RequestInfo info);
};

#endif