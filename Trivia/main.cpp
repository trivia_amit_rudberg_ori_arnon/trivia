#include <iostream>
#include "Server.h"
#include "WSAInitializer.h"
#include "SqliteDatabase.h"

int main()
{
	WSAInitializer wsaInit;
	Server server;
	try
	{
		server.run();

		system("Pause");
	}
	catch (const std::exception& e)
	{
		std::cout << e.what() << std::endl;
	}
}