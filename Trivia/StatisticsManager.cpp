#include "StatisticsManager.h"
std::vector<std::string> StatisticsManager::getHighSore()
{
    return m_database->getHighScores();
}

std::vector<std::string> StatisticsManager::getUserStatistics(std::string username)
{
    std::vector<std::string> statistics;
    statistics.push_back(std::to_string(m_database->getNumOfPlayerGames(username)));
    statistics.push_back(std::to_string(m_database->getNumOfCorrectAnswers(username)));
    statistics.push_back(std::to_string(m_database->getNumOfTotalAnswers(username) - m_database->getNumOfCorrectAnswers(username)));
    statistics.push_back(std::to_string(m_database->getPlayerAverageTime(username)));

    return statistics;
}
