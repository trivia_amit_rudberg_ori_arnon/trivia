#ifndef REQUESTHANDLERFACTORY_H
#define REQUESTHANDLERFACTORY_H
#include "IRequestHandler.h"
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h"
#include "IDatabase.h"
#include "LoginManager.h"
#include "RoomManager.h"
#include "StatisticsManager.h"
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"
#include "GameManager.h"
#include "GameRequestHandler.h"

class LoginRequestHandler;
class MenuRequestHandler;
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;
class GameRequestHandler;

class RequestHandlerFactory
{
public:
	RequestHandlerFactory(IDatabase* db);

	LoginRequestHandler* createLoginRequestHandler();
	MenuRequestHandler* createMenuRequestHandler(LoggedUser& loggedUser);
	RoomAdminRequestHandler* createRoomAdminRequestHandler(LoggedUser& loggedUser, Room& room);
	RoomMemberRequestHandler* createRoomMemberRequestHandler(LoggedUser& loggedUser, Room& room);
	GameRequestHandler* createGameRequestHandler(LoggedUser& loggedUser, Game* game);
	LoginManager& getLoginManager();
	RoomManager& getRoomManager();
	StatisticsManager& getStatisticsManager();
	GameManager& getGameManager();
private:
	IDatabase* m_dataBase;
	LoginManager m_loginManager;
	RoomManager m_roomManager;
	StatisticsManager m_statisticsManager;
	GameManager m_gameManager;
	
};

#endif // !REQUESTHANDLERFACTORY_H

