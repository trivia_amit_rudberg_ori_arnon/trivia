#ifndef IREQUESTHANDLER_H
#define IREQUESTHANDLER_H
#include <iostream>
#include "HandlerStructs.h"
#include "MessageCodes.h"
#include "ResponseStructs.h"
#include "RequestStructs.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"

class IRequestHandler
{
public:
	IRequestHandler() {};

	virtual bool isRequestRelevant(RequestInfo& info) = 0;
	virtual RequestResult handleRequest(RequestInfo& info) = 0;

private:

};

#endif