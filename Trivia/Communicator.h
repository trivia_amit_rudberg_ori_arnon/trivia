#ifndef COMMUNICATOR_H
#define COMMUNICATOR_H
#include <iostream>
#include <WinSock2.h>
#include <map>
#include <thread>
#include "IRequestHandler.h"
#include "LoginRequestHandler.h"
#include "WSAInitializer.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "RequestHandlerFactory.h"

#define PORT 8820

class Communicator
{
public:
	Communicator(RequestHandlerFactory& factory);
	~Communicator();

	void startHandleRequests();

private:
	SOCKET m_serverSocket;
	std::map<SOCKET, IRequestHandler*> m_clients;
	RequestHandlerFactory& m_handlerFactory;

	void bindAndListen();
	void handleNewClient(SOCKET sck);
	void sendError(std::string message, SOCKET sck);
	unsigned int getRequestId(char codeArr[MESSAGE_CODE_LENGTH]);
};

#endif