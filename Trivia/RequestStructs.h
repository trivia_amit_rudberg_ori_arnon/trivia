#ifndef REQUESTSTRUCTS_H
#define REQUESTSTRUCTS_H
#include <string>

struct LoginRequest
{
	std::string username;
	std::string password;
};

struct SignUpRequest
{
	std::string username;
	std::string password;
	std::string email;

};

struct GetPlayersInRoomRequest
{
	unsigned int roomId;
};

struct JoinRoomRequest
{
	unsigned int roomId;
};

struct CreateRoomRequest
{
	std::string roomName;
	unsigned int maxUsers;
	unsigned int questionCount;
	unsigned int answerTimeout;
};

struct SubmitAnswerRequest
{
	unsigned int answerId;
};
#endif