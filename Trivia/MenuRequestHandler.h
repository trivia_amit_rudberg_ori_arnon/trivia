#ifndef MENUREQUESTHANDLER_H
#define MENUREQUESTHANDLER_H
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include "LoggedUser.h"

class RequestHandlerFactory;

class MenuRequestHandler : public IRequestHandler
{
public:
	MenuRequestHandler(RequestHandlerFactory& factory, LoggedUser& user);
	~MenuRequestHandler();

	bool isRequestRelevant(RequestInfo& info) override;
	RequestResult handleRequest(RequestInfo& info) override;
private:
	LoggedUser m_user;
	RequestHandlerFactory& m_handlerFactory;

	RequestResult signOut(RequestInfo& info);
	RequestResult getRooms(RequestInfo& info);
	RequestResult getPlayersInRoom(RequestInfo& info);
	RequestResult getPersonalStats(RequestInfo& info);
	RequestResult getHighScore(RequestInfo& info);
	RequestResult joinRoom(RequestInfo& info);
	RequestResult createRoom(RequestInfo& info);
};

#endif