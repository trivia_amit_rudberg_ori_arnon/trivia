﻿using Client.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (Environment.OSVersion.Version.Major >= 6)
            { SetProcessDPIAware(); }


            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            try
            {
                Communicator.SocketConnect();
                Application.Run(new Menu());
            }
            catch (Exception ex)
            {
                Error e = new Error(ex.Message);
                e.ShowDialog();
            }

            if (Communicator.loggedUserName != "")
            { Request.logout(); }
            Communicator.SocketDisconnect();
        }

        [System.Runtime.InteropServices.DllImport("user32.dll")]
        private static extern bool SetProcessDPIAware();
    }
}
