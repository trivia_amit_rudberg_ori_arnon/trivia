﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ComponentFactory.Krypton.Toolkit;
namespace Client.Views
{
    public partial class Question : KryptonForm
    {
        Menu menu;
        private int secondsToWait = 0;
        int elapsedSeconds = 0;
        private DateTime startTime;
        private Color buttonColor;
        private int questionCount;
        private int questionsAnswerd = 0;
        private Color correctColor = Color.FromArgb(198, 236, 174);
        private Color wrongColor = Color.FromArgb(213, 87, 59);
        public Question(Menu menu, int answerTimeout, int questionCount)
        {
            this.menu = menu;
            InitializeComponent();
            secondsToWait = answerTimeout;
            this.questionCount = questionCount;
            buttonColor = ansr1btn.BackColor;
            this.FormClosed += new FormClosedEventHandler(question_FormClosed);

            getQ();
        }

        private void getQ()
        {

                Q q = Request.get_Question();
                questionText.Text = q.question;
                questionTimer.Value = 100;

                startTime = DateTime.Now;
                clock.Start();

                timerText.Text = q.question;
                timerText.Text = secondsToWait.ToString();

                ansr1btn.Text = q.answers[0];
                ansr2btn.Text = q.answers[1];
                ansr3btn.Text = q.answers[2];
                ansr4btn.Text = q.answers[3];

                ansr1btn.Enabled = true;
                ansr2btn.Enabled = true;
                ansr3btn.Enabled = true;
                ansr4btn.Enabled = true;

                ansr1btn.BackColor = buttonColor;
                ansr2btn.BackColor = buttonColor;
                ansr3btn.BackColor = buttonColor;
                ansr4btn.BackColor = buttonColor;

            
        }

        void question_FormClosed(object sender, FormClosedEventArgs e)
        {
            Request.LeaveGame();

            menu.Show();
        }

        private void clock_Tick(object sender, EventArgs e)
        {
            //int elapsedSeconds = (int)(DateTime.Now - startTime).TotalSeconds;
            //int remainingSeconds = secondsToWait - elapsedSeconds;
            elapsedSeconds++;
            questionTimer.Value = ((secondsToWait - elapsedSeconds) * 100) / secondsToWait;

            timerText.Text = (secondsToWait - elapsedSeconds).ToString();

            if (elapsedSeconds == secondsToWait)
            {
                answerQuestion(99);
            }

        }

        private void ansr1btn_Click(object sender, EventArgs e)
        {
            answerQuestion(0);
        }

        private void ansr2btn_Click(object sender, EventArgs e)
        {
            answerQuestion(1);
        }

        private void ansr3btn_Click(object sender, EventArgs e)
        {
            answerQuestion(2);
        }

        private void ansr4btn_Click(object sender, EventArgs e)
        {
            answerQuestion(3);
        }

        private void answerQuestion(int answerNum)
        {
            clock.Stop();
            elapsedSeconds = 0;
            SubmitAnswer sa = Request.submitAnswer((uint)answerNum);
            switch (sa.correctAnswerId)
            {
                case 0:
                    ansr1btn.BackColor = correctColor;
                    ansr2btn.BackColor = wrongColor;
                    ansr3btn.BackColor = wrongColor;
                    ansr4btn.BackColor = wrongColor;
                    break;

                case 1:
                    ansr1btn.BackColor = wrongColor;
                    ansr2btn.BackColor = correctColor;
                    ansr3btn.BackColor = wrongColor;
                    ansr4btn.BackColor = wrongColor;
                    break;

                case 2:
                    ansr1btn.BackColor = wrongColor;
                    ansr2btn.BackColor = wrongColor;
                    ansr3btn.BackColor = correctColor;
                    ansr4btn.BackColor = wrongColor;
                    break;

                case 3:
                    ansr1btn.BackColor = wrongColor;
                    ansr2btn.BackColor = wrongColor;
                    ansr3btn.BackColor = wrongColor;
                    ansr4btn.BackColor = correctColor;
                    break;

                default:
                    break;
            }

            ansr1btn.Enabled = false;
            ansr2btn.Enabled = false;
            ansr3btn.Enabled = false;
            ansr4btn.Enabled = false;

            System.Threading.Thread.Sleep(1000);

            questionsAnswerd++;
            if (questionsAnswerd == questionCount)
            {
                clock.Stop();
                Ending end = new Ending(menu, questionCount);
                this.Hide();
                end.ShowDialog();
            }
            else
            {
                getQ();
            }
        }
    }
}
