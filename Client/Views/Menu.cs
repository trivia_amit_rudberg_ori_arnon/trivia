﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Client.Views;
using ComponentFactory.Krypton.Toolkit;
namespace Client
{
    public partial class Menu : KryptonForm
    {
        public Menu()
        {
            InitializeComponent();
            this.Hide();
            Login lo = new Login(this);
            lo.ShowDialog();
        }

        ~Menu() 
        { 
            if (Communicator.loggedUserName != "")
            {
                Request.logout();
            }
            Communicator.SocketDisconnect(); 
        }

        private void mystatusButton_Click(object sender, EventArgs e)
        {
            MyStatus su = new MyStatus();
            su.ShowDialog();
        }

        private void bestscoresButton_Click(object sender, EventArgs e)
        {
            BestScores bs = new BestScores();
            bs.ShowDialog();
        }

        private void logoutBtn_Click(object sender, EventArgs e)
        {
            Request.logout();
            Login lg = new Login(this);
            this.Hide();
            lg.ShowDialog();
        }

        private void createroomButton_Click(object sender, EventArgs e)
        {
            CreateRoom cr = new CreateRoom(this);
            this.Hide();
            cr.ShowDialog();
        }

        private void joinroomButton_Click(object sender, EventArgs e)
        {
            JoinRoom jr = new JoinRoom(this);
            this.Hide();
            jr.ShowDialog();
        }
    }
}
