﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ComponentFactory.Krypton.Toolkit;

namespace Client.Views
{
    public partial class Ending : KryptonForm
    {
        Menu menu;
        int questionCount;
        Thread thr;
        bool gameEnded = true;
        public Ending(Menu menu, int questionCount)
        {
            InitializeComponent();
            this.FormClosed += new FormClosedEventHandler(ending_FormClosed);
            this.menu = menu;
            this.questionCount = questionCount;

            waitText.Visible = true;

            resultsText.Visible = false;
            crown.Visible = false;
            leaveGameButton.Visible = false;

            place1.Visible = false;
            place2.Visible = false;
            place3.Visible = false;
            place4.Visible = false;
            place5.Visible = false;
            place6.Visible = false;

            thr = new Thread(this.threadCheck);
            thr.Start();
        }

        private void threadCheck()
        {
            while (!checkForEnd())
            {
                Thread.Sleep(3000);
            }
        }

        private void LeaveGame()
        {
            Request.LeaveGame();
            menu.Show();
        }

        private bool checkForEnd()
        {

            GetGameResultsRespone response = Request.GetGameResults();
            gameEnded = true;
            foreach (PlayerResults pr in response.results) 
            {
                gameEnded = gameEnded && pr.correctAnswerCount + pr.wrongAnswerCount == questionCount;
            }

            if (gameEnded)
            {
                response.results.OrderBy(x => x.correctAnswerCount).ToList().Reverse();

                this.Invoke((MethodInvoker)(() =>
                {
                    waitText.Visible = false;
                    resultsText.Visible = true;
                    leaveGameButton.Visible = true;
                    crown.Visible = true;

                    place1.Visible = true;
                    place1.Text = response.results[0].username + " - " + response.results[0].correctAnswerCount.ToString();

                    if (response.results.Count() > 1)
                    {
                        place2.Visible = true;
                        place2.Text = response.results[1].username + " - " + response.results[1].correctAnswerCount.ToString();
                    }

                    if (response.results.Count() > 2)
                    {
                        place3.Visible = true;
                        place3.Text = response.results[2].username + " - " + response.results[2].correctAnswerCount.ToString();
                    }

                    if (response.results.Count() > 3)
                    {
                        place4.Visible = true;
                        place4.Text = response.results[3].username + " - " + response.results[3].correctAnswerCount.ToString();
                    }

                    if (response.results.Count() > 4)
                    {
                        place5.Visible = true;
                        place5.Text = response.results[4].username + " - " + response.results[4].correctAnswerCount.ToString();
                    }

                    if (response.results.Count() > 5)
                    {
                        place6.Visible = true;
                        place6.Text = response.results[5].username + " - " + response.results[5].correctAnswerCount.ToString();
                    }

                }));
                return true;
                
            }

            return false;
        }



        void ending_FormClosed(object sender, FormClosedEventArgs e)
        {
            LeaveGame();
        }

        private void leaveGameButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            LeaveGame();
        }
    }
}
