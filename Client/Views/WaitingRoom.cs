﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ComponentFactory.Krypton.Toolkit;
namespace Client.Views
{
    public partial class WaitingRoom : KryptonForm
    {
        Menu menu = null;
        bool admin = false;
        string roomName;
        int roomId;
        int playerCount = 0;
        int answerTimeOut;
        List<string> players;
        Thread thr;
        bool roomRunning = true;
        int questionCount = 0;
        bool close = false;
        public WaitingRoom(Menu menu, bool admin, string roomName, int roomId, int playerCount, int answerTimeOut)
        {
            InitializeComponent();
            this.menu = menu;
            this.admin = admin;
            this.roomName = roomName;
            this.playerCount = playerCount;
            this.answerTimeOut = answerTimeOut;
            this.FormClosed += new FormClosedEventHandler(waitingroom_FormClosed);
            this.roomId = roomId;
            players = new List<string>();
            thr = new Thread(this.refreshTheNames);
            thr.Start();

            playButton.Enabled = admin;
            roomnameText.Text = roomName;
            this.answerTimeOut = answerTimeOut;
        }

        void waitingroom_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (roomRunning)
            {
                
                close = true;
                if (admin)
                {
                    Request.closeRoom();
                }
                else
                {
                    Request.leaveRoom();
                }
            }
            menu.Show();
        }

        private void playButton_Click(object sender, EventArgs e)
        {
            if (Request.startGame() == 0)
            {
                close = true;
                beginGame();
            }
        }

        void beginGame()
        {
            roomRunning = false;
            this.Hide();
            Question q = new Question(menu, answerTimeOut, questionCount);
            q.ShowDialog();
        }

        bool updateNames()
        {
           // close = true;
                RoomState rs = Request.getRoomState();

                this.questionCount = rs.questionCount;
                this.answerTimeOut = (int)rs.answerTimeout;

                if (rs.hasGameBegun)
                {
                    this.Invoke((MethodInvoker)(() =>
                    {
                        beginGame();
                    }));
                    return true;
                }

                

                foreach (string p in rs.players)
                {
                    if (!playerList.Items.Contains(p))
                    {
                        playerList.Invoke((MethodInvoker)(() =>
                        {
                            playerList.Items.Add(p);
                        }));
                    }
                }

                foreach (string p in players)
                {
                    if (!rs.players.Contains(p))
                    {
                        playerList.Invoke((MethodInvoker)(() =>
                        {
                            playerList.Items.Remove(p);
                        }));
                        //playerList.Items.Add(p);
                    }
                }

                players = rs.players;

                if (rs.status == 1)
                {
                    roomRunning = false;
                    playerList.Items.Remove(Communicator.loggedUserName);
                    roomClosedText.Invoke((MethodInvoker)(() =>
                    {
                        roomClosedText.Visible = true;
                    }));
                    return true;
                }


                if (admin && rs.players.Count() >= playerCount)
                {
                    playButton.Invoke((MethodInvoker)(() =>
                    {
                         playButton.Enabled = true;
                    }));
                }
                else
                {
                    playButton.Invoke((MethodInvoker)(() =>
                    {
                        playButton.Enabled = false;
                    }));
                }
            return false;
        }

        void refreshTheNames()
        {
            while (!close && !updateNames())
            {
                //close = true;
                Thread.Sleep(3000);
            }
        }

    }
}
