﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ComponentFactory.Krypton.Toolkit;
namespace Client
{
    public partial class Login : KryptonForm
    {
        public Menu menu = null;

        public Login(Menu men)
        {
            InitializeComponent();
            this.FormClosed += new FormClosedEventHandler(login_FormClosed);
            menu = men;
        }

        private void loginButton_Click(object sender, EventArgs e)
        {
            int status =Request.login(UserNameInputBox.Text, passwordInputBox.Text);
            if (status != 1)
            {
                this.Hide();
                menu.Show();
            }
            else { ErrorMsg.Visible = true; }
        }

        private void signupBtn_Click(object sender, EventArgs e)
        {
            SignUp su = new SignUp(this);
            this.Hide();
            su.ShowDialog();
        }
        void login_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
