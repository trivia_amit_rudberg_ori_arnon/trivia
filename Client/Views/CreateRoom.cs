﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ComponentFactory.Krypton.Toolkit;
static class Constants
{
    
    public const uint MaxQuestions = 20;
    public const uint Maxplayer = 6;
    public const uint MaxTime = 99;
}


namespace Client.Views
{

    public partial class CreateRoom : KryptonForm
    {
        Menu menu = null;
        public CreateRoom(Menu menu)
        {
            InitializeComponent();
            this.FormClosed += new FormClosedEventHandler(closeRoom_FormClosed);
            this.menu = menu;
        }

        private void menuButton_Click(object sender, EventArgs e)
        {
            menu.Show();
            this.Hide();
        }


        void closeRoom_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void createRoomButton_Click(object sender, EventArgs e)
        {
            try
            {
                if (uint.Parse(questionAmmountText.Text) > Constants.MaxQuestions || uint.Parse(playerammountText.Text) > Constants.Maxplayer || uint.Parse(playerammountText.Text) == 0 || uint.Parse(timeText.Text) > Constants.MaxTime || roomNameText.Text.Length == 0 )
                {
                    throw new Exception();
                }
                int status =Request.createRoom(roomNameText.Text, uint.Parse(playerammountText.Text), uint.Parse(questionAmmountText.Text), uint.Parse(timeText.Text));
                if (status != 1)
                {

                    WaitingRoom wr = new WaitingRoom(menu, true, roomNameText.Text, Request.getRoomState().id, Math.Abs(int.Parse(playerammountText.Text)), int.Parse(timeText.Text));
                    wr.Show();
                    this.Hide();
                }
                else { ErrorMsg.Visible = true; }
            }
            catch
            {
                ErrorMsg.Visible = true;
                
            }
        }
    }
}
