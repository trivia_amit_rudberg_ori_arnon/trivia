﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ComponentFactory.Krypton.Toolkit;
namespace Client.Views
{
    public partial class MyStatus : KryptonForm
    {
        public MyStatus()
        {
            InitializeComponent();
            List<double> ps = Request.personalStats();
            NOGtext.Text = ps[0].ToString();
            NORGtext.Text = ps[1].ToString();
            NOWAtext.Text = ps[2].ToString();
            ATFAtext.Text = Math.Round(ps[3], 2).ToString();
        }
    }
}
