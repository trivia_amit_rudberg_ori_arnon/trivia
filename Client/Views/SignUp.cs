﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ComponentFactory.Krypton.Toolkit;
namespace Client
{
    

    public partial class SignUp : KryptonForm
    {
        Login login { get; set; }

        public SignUp(Login login)
        {
            InitializeComponent();
            this.login = login;
            this.FormClosed += new FormClosedEventHandler(signup_FormClosed);
       
        }


        private void loginButton_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            login.Show();
        }
        void signup_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void SignUpBtn_Click(object sender, EventArgs e)
        {
            if (UserNameInputBox.Text.Length == 0 || passwordInputBox.Text.Length == 0 || EmailTextBox.Text.Length == 0) 
            {
                ErrorMsg.Visible = true;
            }
            else
            {
                int status = Request.singup(UserNameInputBox.Text, passwordInputBox.Text, EmailTextBox.Text);
                if (status != 1)
                {
                    this.Hide();
                }
                else { ErrorMsg.Visible = true; }

            }
            
        }
    }
}
