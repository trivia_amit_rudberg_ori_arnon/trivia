﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ComponentFactory.Krypton.Toolkit;

namespace Client.Views
{
    public partial class JoinRoom : KryptonForm
    {
        Menu menu = null;
        string selected = "";
        Dictionary<int, string> rooms;
        Thread thr;
        bool joined = false;
        bool close = false;
        public JoinRoom(Menu menu)
        {
            InitializeComponent();
            this.menu = menu;
            this.FormClosed += new FormClosedEventHandler(joinroom_FormClosed);

            
            thr = new Thread(this.refreshTheRooms);
            thr.Start();

        }

        void joinroom_FormClosed(object sender, FormClosedEventArgs e)
        {
            
            close = true;
            menu.Show();
            this.Hide();
        }

        private void menuButton_Click(object sender, EventArgs e)
        {
            close = true;
            menu.Show();
            this.Hide();
            
        }

        private void roomList_SelectedIndexChanged(object sender, EventArgs e)
        {
            selected = roomList.SelectedItem.ToString();
        }

        private void joinroomButton_Click(object sender, EventArgs e)
        {
            if (selected != "") 
            {
                joined = true;
                int id = rooms.FirstOrDefault(x => x.Value == selected).Key;
                Request.joinRoom(id);
                this.Hide();
                WaitingRoom wr = new WaitingRoom(menu, false, selected, id, 0, 0);
                wr.ShowDialog();
            }
            else
            {
                ErrorMsg.Visible = true;
            }

        }

        void updateRooms()
        {
            
            rooms = Request.getRooms();
            

            foreach (KeyValuePair<int, string> room in rooms)
            {
                if (!roomList.Items.Contains(room.Value))
                {
                    roomList.Invoke((MethodInvoker)(() =>
                    {
                        roomList.Items.Add(room.Value);
                    }));
                }
            }

            for (int i = 0; i < roomList.Items.Count; i++)
            {
                if (!rooms.ContainsValue((string)roomList.Items[i]))
                {
                    roomList.Invoke((MethodInvoker)(() =>
                    {
                        roomList.Items.Remove(roomList.Items[i]);
                    }));
                }
            }
        }
        void refreshTheRooms()
        {
            while (!close && !joined )
            {
               updateRooms();
               Thread.Sleep(3000);
            }
        }

    }
}
