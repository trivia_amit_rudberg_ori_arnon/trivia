﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ComponentFactory.Krypton.Toolkit;
namespace Client.Views
{
    public partial class Error : KryptonForm
    {
        public Error(string details)
        {

            Communicator.showingError = true;
            InitializeComponent();
            detailsText.Text = details;
            this.FormClosed += new FormClosedEventHandler(error_FormClosed);
            //this.Close();
        }

        void error_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }
    }
}
