﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ComponentFactory.Krypton.Toolkit;
namespace Client.Views
{
    public partial class BestScores : KryptonForm
    {
        public BestScores()
        {
            InitializeComponent();
            List<string> hs = Request.highScore();

            noPlaysText.Visible = hs.Count == 0;
            if (hs.Count() >= 1) { place1.Text = hs[0]; } else { place1.Visible = false; }
            if (hs.Count() >= 2) { place2.Text = hs[1]; } else { place2.Visible = false; }
            if (hs.Count() >= 3) { place3.Text = hs[2]; } else { place3.Visible = false; }
            if (hs.Count() >= 4) { place4.Text = hs[3]; } else { place4.Visible = false; }
            
        }
    }
}
