﻿



using Client.Views;
using System;
using System.IO;

namespace Client
{
    class config
    {
        public static void getDataConfig (ref string ip,ref int port) 
        {
            try
            {
                string data = File.ReadAllText("config.txt");
                string[] Lines= data.Split('\n');
                ip = Lines[0].Substring(Lines[0].IndexOf("server_ip=") +("server_ip=").Length).Trim();
                port =Convert.ToInt32 (Lines[1].Substring(Lines[1].IndexOf("port=") + ("port=").Length).Trim());

            }
            catch ( Exception e)
            {
                Error err = new Error("ERROR the file don't exists or the format is wrong");
                err.ShowDialog();
            }
        }
    }
}
