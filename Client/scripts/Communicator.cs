﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Client.Views;
using System.Runtime.CompilerServices;
using System.Windows.Forms;

namespace Client
{
    static class Communicator
    {
        static Socket m_socket = null;
        public static string loggedUserName = "";
        public static bool showingError = false;

        public static void SocketConnect()
        {
 
                int port = 0;
                string ip ="";
                config.getDataConfig(ref ip,ref  port);
                byte[] bytes = new byte[1024];
                IPAddress ipAddr = IPAddress.Parse(ip);
                IPEndPoint remoteEP = new IPEndPoint(ipAddr, port);
                m_socket = new Socket(ipAddr.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
                m_socket.Connect(remoteEP);
                Console.WriteLine("Socket connected to " + m_socket.RemoteEndPoint.ToString());

        
        }

        public static void sendMessage(string message)
        {

                if (!(m_socket.Poll(1000, SelectMode.SelectRead) && m_socket.Available == 0))
                {
                    byte[] msg = Encoding.ASCII.GetBytes(message);
                    m_socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveBuffer, 0);
                    m_socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendBuffer, 0);
                    int bytesSent = m_socket.Send(msg);
                }

        }

        public static string recvMessage()
        {

                byte[] bytes = new byte[1024];
                int bytesRec = m_socket.Receive(bytes);
                if (getMessageCode(Encoding.ASCII.GetString(bytes, 0, bytesRec)) == (int)MessageCodes.ERROR_CODE)
                {
                    Error err = new Error("error sent from server");
                    err.ShowDialog();
                }
                string result = Encoding.ASCII.GetString(bytes, 0, bytesRec);
                m_socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveBuffer, 0);
                m_socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendBuffer, 0);
                return result;
            
        }

        public static void SocketDisconnect()
        {
            m_socket.Close();
        }

        public static int getMessageCode(string message)
        {
             return int.Parse(message.Substring(0, 3));

        }
    }
}
