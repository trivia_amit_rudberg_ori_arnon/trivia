﻿using Client.Views;
using ComponentFactory.Krypton.Navigator;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Net.Sockets;
using System.Numerics;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{

    internal class RoomState
    {
        public int id;
        public int status;
        public bool hasGameBegun;
        public List<string> players;
        public int questionCount;
        public double answerTimeout;

        public RoomState()
        {
            id = -1;
            status = 0;
            hasGameBegun = false;
            players = new List<string>();
            questionCount = 0;
            answerTimeout = 0;
        }
    }

    public class Q
    {
        public string question;
        public Dictionary<uint ,string> answers;

        public Q()
        {
              answers = new Dictionary<uint, string>();
        }
    }

    internal class SubmitAnswer
    {
        public uint status;
        public uint correctAnswerId;
        
        public SubmitAnswer()
        {
            status = 0;
        }
    }

    internal class PlayerResults
    {
        public string username;
        public uint correctAnswerCount;
        public uint wrongAnswerCount;
        public uint averageAnswerTime;
        public PlayerResults()
        {
            correctAnswerCount = 0;
            wrongAnswerCount = 0;
            averageAnswerTime = 0;
        }
    }

    internal class GetGameResultsRespone
    {
        public uint status;
        public List<PlayerResults> results;

        public GetGameResultsRespone()
        {
            status = 0;
        }
    }
    internal static class Request
    {

        public static RoomState getRoomState()
        {
                var credentials = new { };
                string json = JsonSerializer.Serialize(credentials);
                string len = (json.Length).ToString();
                Request.padding(ref len);
                Communicator.sendMessage(((int)MessageCodes.ROOM_STATE_CODE).ToString() + len + json);
                string message = Communicator.recvMessage();

                RoomState rs = new RoomState();


                int braceIndex = message.IndexOf('{');
                if (braceIndex == -1)
                {
                    return rs;
                }

                string jsonSubstring = message.Substring(braceIndex);
                JsonDocument jsonDocument = JsonDocument.Parse(jsonSubstring);
                JsonElement root = jsonDocument.RootElement;

                JsonElement playersElement = root.GetProperty("players");
                if (playersElement.ValueKind == JsonValueKind.Array)
                {

                    foreach (JsonElement roomElement in playersElement.EnumerateArray())
                    {
                        rs.players.Add(roomElement.GetString());
                    }
                }

                rs.status = root.GetProperty("id").GetInt32();
                rs.status = root.GetProperty("status").GetInt32();
                rs.questionCount = root.GetProperty("questionCount").GetInt32();
                rs.hasGameBegun = root.GetProperty("hasGameBegun").GetBoolean();
                rs.answerTimeout = root.GetProperty("answerTimeout").GetDouble();

                return rs;
        }

        public static int login(string username, string password)
        {
            var credentials = new
            {
                username = username,
                password = password
            };

            // Serialize the anonymous object to JSON
            string msgJSON = JsonSerializer.Serialize(credentials);

            // Output the JSON string
            string size = (Encoding.UTF8.GetByteCount(msgJSON)).ToString();
            padding(ref size);

            //Console.WriteLine("Size of msgJSON: " + size + " bytes");

            Communicator.sendMessage(((int)(MessageCodes.LOGIN_CODE)).ToString() + size + msgJSON);
            Communicator.loggedUserName = username;
            return statusMessages();

        }

        public static int singup(string username, string password, string email)
        {
            var credentials = new
            {
                username = username,
                password = password,
                email=email
            };

            // Serialize the anonymous object to JSON
            string msgJSON = JsonSerializer.Serialize(credentials);

            // Output the JSON string
            //Console.WriteLine(msgJSON);
            string size = (Encoding.UTF8.GetByteCount(msgJSON)).ToString();
            padding(ref size);

            //Console.WriteLine("Size of msgJSON: " + size + " bytes");

            Communicator.sendMessage(((int)(MessageCodes.SIGNUP_CODE)).ToString() + size + msgJSON);

            return statusMessages();
        }

        public static void logout()
        {
            Communicator.loggedUserName = "";
            var credentials = new
            {
                
            };

            // Serialize the anonymous object to JSON
            string msgJSON = JsonSerializer.Serialize(credentials);

            // Output the JSON string
            //Console.WriteLine(msgJSON);
            string size = (Encoding.UTF8.GetByteCount(msgJSON)).ToString();
            padding(ref size);

            //Console.WriteLine("Size of msgJSON: " + size + " bytes");

            Communicator.sendMessage(((int)(MessageCodes.LOGOUT_CODE)).ToString() + size + msgJSON);
            Communicator.recvMessage();
        }


        public static Dictionary<int, string> getRooms()
        {
            var credentials = new
                {

                };

                // Serialize the anonymous object to JSON
                string msgJSON = JsonSerializer.Serialize(credentials);

                // Output the JSON string
                //Console.WriteLine(msgJSON);
                string size = (Encoding.UTF8.GetByteCount(msgJSON)).ToString();
                padding(ref size);

                //Console.WriteLine("Size of msgJSON: " + size + " bytes");

                Communicator.sendMessage(((int)(MessageCodes.GET_ROOMS_CODE)).ToString() + size + msgJSON);

                string message = Communicator.recvMessage();
                Dictionary<int, string> rooms = new Dictionary<int, string>();
                int braceIndex = message.IndexOf('{');
                if (braceIndex == -1)
                {

                    return rooms;
                }
                string jsonSubstring = message.Substring(braceIndex);
                JsonDocument jsonDocument = JsonDocument.Parse(jsonSubstring);
                JsonElement root = jsonDocument.RootElement;
                JsonElement roomsElement = root.GetProperty("rooms");


                if (roomsElement.ValueKind == JsonValueKind.Array)
                {
                    
                    // Iterate over the array and extract room names
                    foreach (JsonElement roomElement in roomsElement.EnumerateArray())
                    {
                        bool first = true;
                        int key = 0;
                        string val = "";
                        foreach(JsonElement room in roomElement.EnumerateArray())
                        {
                            if (first)
                            {
                                key = room.GetInt32();
                                first = false;
                            }
                            else
                            {
                                val = room.GetString();
                            }

                        }
                        rooms[key] = val;
                    }
                }
                return rooms;

        }

        /*
        public static List<string> getPlayersInTheRoom(int roomId)
        {
            try
            {
                var credentials = new
                {
                    roomId=roomId
                };

                // Serialize the anonymous object to JSON
                string msgJSON = JsonSerializer.Serialize(credentials);

                // Output the JSON string
                //Console.WriteLine(msgJSON);
                string size = (Encoding.UTF8.GetByteCount(msgJSON)).ToString();
                padding(ref size);

                //Console.WriteLine("Size of msgJSON: " + size + " bytes");

                Communicator.sendMessage(((int)(MessageCodes.PLAYERS_IN_ROOM_CODE)).ToString() + size + msgJSON);

            
                string message = Communicator.recvMessage();
                List<string> Players = new List<string>();


                int braceIndex = message.IndexOf('{');
                if (braceIndex == -1)
                {
                    // Return an empty list if opening curly brace not found
                    return Players;
                }

                // Get the substring starting from the opening curly brace
                string jsonSubstring = message.Substring(braceIndex);

                // Try to parse the JSON substring

                JsonDocument jsonDocument = JsonDocument.Parse(jsonSubstring);


                JsonElement root = jsonDocument.RootElement;


                JsonElement roomsElement = root.GetProperty("players");


                if (roomsElement.ValueKind == JsonValueKind.Array)
                {

                    foreach (JsonElement roomElement in roomsElement.EnumerateArray())
                    {
                        Players.Add(roomElement.GetString());
                    }
                }
                return Players;
            }
            catch (Exception e)
            {
                List<string> Players = new List<string>();
                Error err = new Error(e.Message);
                err.ShowDialog();
                return Players;
            }
        }
        */
        public static List<string> highScore()
        {
            List<string> result = new List<string>();
            var credentials = new
            {

            };

            // Serialize the anonymous object to JSON
            string msgJSON = JsonSerializer.Serialize(credentials);

            // Output the JSON string
            //Console.WriteLine(msgJSON);
            string size = (Encoding.UTF8.GetByteCount(msgJSON)).ToString();
            padding(ref size);

            //Console.WriteLine("Size of msgJSON: " + size + " bytes");

            Communicator.sendMessage(((int)(MessageCodes.HIGH_SCORE_CODE)).ToString() + size + msgJSON);

            string msg = Communicator.recvMessage();
            JsonDocument jsonDocument = JsonDocument.Parse(msg.Substring(msg.IndexOf("{")));

            // Get the root element of the JSON document
            JsonElement root = jsonDocument.RootElement;

            // Get the "rooms" property
            JsonElement roomsElement = root.GetProperty("statistics");

            // Check if the "rooms" property is an array
            if (roomsElement.ValueKind == JsonValueKind.Array)
            {
                // Iterate over the array and extract room names
                foreach (JsonElement roomElement in roomsElement.EnumerateArray())
                {
                    result.Add(roomElement.GetString());
                }
            }

            return result;
        }

        public static List<double> personalStats()
        {
            List<double> result = new List<double>();
            var credentials = new
            {

            };

            // Serialize the anonymous object to JSON
            string msgJSON = JsonSerializer.Serialize(credentials);

            // Output the JSON string
            //Console.WriteLine(msgJSON);
            string size = (Encoding.UTF8.GetByteCount(msgJSON)).ToString();
            padding(ref size);

            //Console.WriteLine("Size of msgJSON: " + size + " bytes");

            Communicator.sendMessage(((int)(MessageCodes.PERSONAL_STATS_CODE)).ToString() + size + msgJSON);

            string msg = Communicator.recvMessage();
            JsonDocument jsonDocument = JsonDocument.Parse(msg.Substring(msg.IndexOf("{")));

            // Get the root element of the JSON document
            JsonElement root = jsonDocument.RootElement;

            // Get the "rooms" property
            JsonElement roomsElement = root.GetProperty("statistics");

            // Check if the "rooms" property is an array
            if (roomsElement.ValueKind == JsonValueKind.Array)
            {
                // Iterate over the array and extract room names
                foreach (JsonElement roomElement in roomsElement.EnumerateArray())
                {
                    result.Add(roomElement.GetDouble());
                }
            }
            return result;
        }


        public static void joinRoom(int roomId)
        {
            var credentials = new
            {
                roomId =roomId
            };

            // Serialize the anonymous object to JSON
            string msgJSON = JsonSerializer.Serialize(credentials);

            // Output the JSON string
            //Console.WriteLine(msgJSON);
            string size = (Encoding.UTF8.GetByteCount(msgJSON)).ToString();
            padding(ref size);

            //Console.WriteLine("Size of msgJSON: " + size + " bytes");

            Communicator.sendMessage(((int)(MessageCodes.JOIN_ROOM_CODE)).ToString() + size + msgJSON);
            Communicator.recvMessage();
        }

        public static int closeRoom()
        {
            var credentials = new { };

            string msgJSON = JsonSerializer.Serialize(credentials);

            string size = (Encoding.UTF8.GetByteCount(msgJSON)).ToString();
            padding(ref size);

            Communicator.sendMessage(((int)(MessageCodes.CLOSE_ROOM_CODE)).ToString() + size + msgJSON);
            return statusMessages();
        }

        public static int leaveRoom()
        {
            var credentials = new { };

            string msgJSON = JsonSerializer.Serialize(credentials);

            string size = (Encoding.UTF8.GetByteCount(msgJSON)).ToString();
            padding(ref size);

            Communicator.sendMessage(((int)(MessageCodes.LEAVE_ROOM_CODE)).ToString() + size + msgJSON);
            return statusMessages();
        }

        public static int startGame()
        {
            var credentials = new { };

            string msgJSON = JsonSerializer.Serialize(credentials);

            string size = (Encoding.UTF8.GetByteCount(msgJSON)).ToString();
            padding(ref size);

            Communicator.sendMessage(((int)(MessageCodes.START_GAME_CODE)).ToString() + size + msgJSON);
            return statusMessages();
        }

        public static int createRoom(string roomName,uint MaxUser,uint questionCount,uint answerTimeout)
        {
            var credentials = new
            {
                roomName=roomName,
                MaxUser=MaxUser,
                questionCount=questionCount,
                answerTimeout=answerTimeout
            };

            // Serialize the anonymous object to JSON
            string msgJSON = JsonSerializer.Serialize(credentials);

            // Output the JSON string
            //Console.WriteLine(msgJSON);
            string size = (Encoding.UTF8.GetByteCount(msgJSON)).ToString();
            padding(ref size);

            //Console.WriteLine("Size of msgJSON: " + size + " bytes");

            Communicator.sendMessage(((int)(MessageCodes.CREATE_ROOM_CODE)).ToString() + size + msgJSON);
            return statusMessages();
        }


        public static int LeaveGame()
        {
            var credentials = new { };

            string msgJSON = JsonSerializer.Serialize(credentials);

            string size = (Encoding.UTF8.GetByteCount(msgJSON)).ToString();
            padding(ref size);

            Communicator.sendMessage(((int)(MessageCodes.LEAVE_GAME_CODE)).ToString() + size + msgJSON);
            return statusMessages();
        }


        public static Q get_Question()
        {
                var credentials = new { };

                string msgJSON = JsonSerializer.Serialize(credentials);

                string size = (Encoding.UTF8.GetByteCount(msgJSON)).ToString();
                padding(ref size);

                Communicator.sendMessage(((int)(MessageCodes.GET_QUESTION_CODE)).ToString() + size + msgJSON);

                string message = Communicator.recvMessage();

                Q rs = new Q();



                int braceIndex = message.IndexOf('{');
                if (braceIndex == -1)
                {
                    return rs;
                }

                string jsonSubstring = message.Substring(braceIndex);
                JsonDocument jsonDocument = JsonDocument.Parse(jsonSubstring);
                JsonElement root = jsonDocument.RootElement;

                JsonElement questions = root.GetProperty("answers");


                foreach (JsonElement answerElement in questions.EnumerateArray())
                {
                    uint id = 0;
                    string value = "";
                    bool key = true;
                    foreach (JsonElement je in answerElement.EnumerateArray())
                    {
                        if (key)
                        {
                            key = false;
                            id = je.GetUInt32();
                        }
                        else
                        {
                            value = je.GetString();
                        }
                    }

                    rs.answers[id] = value;
                }

                rs.question = root.GetProperty("question").ToString();
                return rs;

        }



        public static SubmitAnswer submitAnswer(uint answerid)
        {

                var credentials = new {
                    answerid = answerid
                };

                string msgJSON = JsonSerializer.Serialize(credentials);

                string size = (Encoding.UTF8.GetByteCount(msgJSON)).ToString();
                padding(ref size);

                Communicator.sendMessage(((int)(MessageCodes.SUBMIT_ANSWER_CODE)).ToString() + size + msgJSON);

                string message = Communicator.recvMessage();

                SubmitAnswer rs = new SubmitAnswer();



                int braceIndex = message.IndexOf('{');
                if (braceIndex == -1)
                {
                    return rs;
                }

                string jsonSubstring = message.Substring(braceIndex);
                JsonDocument jsonDocument = JsonDocument.Parse(jsonSubstring);
                JsonElement root = jsonDocument.RootElement;


                rs.status = root.GetProperty("status").GetUInt32();
                rs.correctAnswerId = root.GetProperty("correctAnswerId").GetUInt32();
                return rs;

        }

        public static GetGameResultsRespone GetGameResults()
        {
                var credentials = new { };
                

                string msgJSON = JsonSerializer.Serialize(credentials);

                string size = (Encoding.UTF8.GetByteCount(msgJSON)).ToString();
                padding(ref size);

                Communicator.sendMessage(((int)(MessageCodes.GAME_RESULTS_CODE)).ToString() + size + msgJSON);

                string message = Communicator.recvMessage();

                GetGameResultsRespone rs = new GetGameResultsRespone();



                int braceIndex = message.IndexOf('{');
                if (braceIndex == -1)
                {
                    return rs;
                }

                string jsonSubstring = message.Substring(braceIndex);
                JsonDocument jsonDocument = JsonDocument.Parse(jsonSubstring);
                JsonElement root = jsonDocument.RootElement;
                JsonElement resultsElement = root.GetProperty("results");

                List<PlayerResults> result = new List<PlayerResults>();

                if (resultsElement.ValueKind == JsonValueKind.Array)
                {

                    foreach (JsonElement roomElement in resultsElement.EnumerateArray())
                    {
                        PlayerResults playerResults = new PlayerResults();
                        int i = 0;
                        foreach( JsonElement je in roomElement.EnumerateArray())
                        {
                            if (i == 0)
                            {
                                playerResults.username = je.GetString();
                            }
                            else if (i == 1)
                            {
                                playerResults.averageAnswerTime = je.GetUInt32();
                            }
                            else if (i == 2)
                            {
                                playerResults.correctAnswerCount = je.GetUInt32();
                            }
                            else
                            {
                                playerResults.wrongAnswerCount = je.GetUInt32();
                            }

                            i++;
                        }
                        result.Add(playerResults);
                    }
                }

               
                rs.status = root.GetProperty("status").GetUInt32();
                rs.results = result;
                
                return rs;

        }
        public static void  padding(ref string length)
        {
             length = "0".PadLeft(4 - Math.Min(4, length.Length), '0') + length;
        }


        public static int statusMessages()
        {
                string message = Communicator.recvMessage();
                int braceIndex = message.IndexOf('{');
                string jsonString = message.Substring(braceIndex);
                JsonDocument jsonObject = JsonDocument.Parse(jsonString);
                JsonElement element = jsonObject.RootElement.GetProperty("status");
                return element.GetInt32();
        }
    }
}
