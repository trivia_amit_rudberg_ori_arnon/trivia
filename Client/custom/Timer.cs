﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//using System.Windows.Forms;
//using System.Drawing;
//using System.Drawing.Drawing2D;
//using System.ComponentModel;

//namespace Client.custom
//{
//    public enum textPosition
//    {
//        left,
//        right,
//        centered,
//        sliding,
//        none
//    }
//    internal class Timer : ProgressBar
//    {
//        private Color channelColor = Color.FromArgb(15, 17, 15);
//        private Color sliderColor = Color.FromArgb(244, 227, 178);
//        private Color foreBackColor = Color.FromArgb(244, 227, 178);

//        private int channelHeight = 4;
//        private int sliderHeight = 3;
//        private textPosition showValue = textPosition.left;

//        private bool paintedBack = false;
//        private bool stopPainting = false;

//        public Timer()
//        {
//            this.SetStyle(ControlStyles.UserPaint, true);
//            this.ForeColor = Color.White;
//        }

//        [Category("custom timer")]
//        public Color ChannelColor { get => channelColor; set { channelColor = value; this.Invalidate(); } }
//        [Category("custom timer")]
//        public Color SliderColor { get => sliderColor; set { sliderColor = value; this.Invalidate(); } }
//        [Category("custom timer")]
//        public Color ForeBackColor { get => foreBackColor; set { foreBackColor = value; this.Invalidate(); } }
//        [Category("custom timer")]
//        public int ChannelHeight { get => channelHeight; set { channelHeight = value; this.Invalidate(); } }
//        [Category("custom timer")]
//        public int SliderHeight { get => sliderHeight; set { sliderHeight = value; this.Invalidate(); } }
//        [Category("custom timer")]
//        public textPosition ShowValue { get => showValue; set { showValue = value; this.Invalidate(); } }

//        [Category("custom timer")]
//        [Browsable(true)]
//        [EditorBrowsable(EditorBrowsableState.Always)]
//        public override Font Font { get => base.Font; set => base.Font = value; }
//        [Category("custom timer")]
//        public override Color ForeColor { get => base.ForeColor; set => base.ForeColor = value; }

//        protected override void OnPaintBackground(PaintEventArgs pevent)
//        {
//            if (!stopPainting)
//            {
//                if (!paintedBack)
//                {
//                    Graphics graph = pevent.Graphics;
//                    Rectangle rectChannel = new Rectangle(0, 0, Width, channelHeight);
//                    using (var brushChannel = new SolidBrush(channelColor))
//                    {
//                        rectChannel.Y = (channelHeight >= sliderHeight) ? Height - channelHeight : Height - ((channelHeight + sliderHeight) / 2);

//                        graph.Clear(this.Parent.BackColor);
//                        graph.FillRectangle(brushChannel, rectChannel);

//                        if (!DesignMode)
//                        {
//                            paintedBack = true;
//                        }
//                    }
//                }

//                if (Value == Maximum || Value == Minimum)
//                {
//                    paintedBack = false;
//                }

//            }
//        }

//        protected override void OnPaint(PaintEventArgs e)
//        {
//            if (!stopPainting)
//            {
//                Graphics graph = e.Graphics;
//                double scaleFactor = (((double)Value - Minimum)/((double)Maximum - Minimum));
//                int sliderWidth = (int)(scaleFactor * Width);
//                Rectangle rectSlider = new Rectangle(0, 0, sliderWidth, sliderHeight);
//                using (var brushSlider = new SolidBrush(sliderColor))
//                {
//                    rectSlider.Y = (sliderHeight >= channelHeight) ? Height - sliderHeight : Height - ((sliderHeight + channelHeight) / 2);

//                    if (sliderWidth > 1) { graph.FillRectangle(brushSlider, rectSlider); }

//                    if (showValue != textPosition.none)
//                    {
//                        drawValueText(graph, sliderWidth, rectSlider);
//                    }
//                }
//            }
//        }

//        private void drawValueText(Graphics graph, int sliderWidth, Rectangle rectSlider)
//        {
//            string text = Value.ToString() + "%";
//            var textSize = TextRenderer.MeasureText(text, Font);
//            var rectText = new Rectangle(0, 0, textSize.Width, textSize.Height);
//            using (var brushText = new SolidBrush(this.ForeColor))
//            using (var brushTextBack = new SolidBrush(ForeBackColor))
//            using (var textFormat = new StringFormat())
//            {
//                switch (showValue)
//                {
//                    case textPosition.left:
//                        rectText.X = 0;
//                        textFormat.Alignment = StringAlignment.Near;
//                        break;
//                    case textPosition.right:
//                        rectText.X = Width - textSize.Width;
//                        textFormat.Alignment = StringAlignment.Far;
//                        break;
//                    case textPosition.centered:
//                        rectText.X = (Width - textSize.Width) / 2;
//                        textFormat.Alignment = StringAlignment.Center;
//                        break;
//                    case textPosition.sliding:
//                        rectText.X = sliderWidth - textSize.Width;
//                        textFormat.Alignment = StringAlignment.Center;

//                        using(var brushClear = new SolidBrush(Parent.BackColor))
//                        {
//                            var rect = rectSlider;
//                            rect.Y = rectText.Y;
//                            graph.FillRectangle(brushClear, rect);
//                        }
//                        break;

//                }

//                graph.FillRectangle(brushTextBack, rectText);
//                graph.DrawString(text, Font, brushText, rectText, textFormat);

//            }
//        }
//    }
//}

using System;
using System.Drawing;
using System.Windows.Forms;
using System.ComponentModel;

namespace Client.custom
{
    public enum textPosition
    {
        left,
        right,
        centered,
        sliding,
        none
    }

    internal class Timer : ProgressBar
    {
        private Color channelColor = Color.FromArgb(15, 17, 15);
        private Color sliderColor = Color.FromArgb(244, 227, 178);
        private Color foreBackColor = Color.FromArgb(244, 227, 178);

        private int channelHeight = 4;
        private int sliderHeight = 3;
        private textPosition showValue = textPosition.left;

        public Timer()
        {
            this.SetStyle(ControlStyles.UserPaint, true);
            this.ForeColor = Color.White;
        }

        [Category("custom timer")]
        public Color ChannelColor { get => channelColor; set { channelColor = value; this.Invalidate(); } }
        [Category("custom timer")]
        public Color SliderColor { get => sliderColor; set { sliderColor = value; this.Invalidate(); } }
        [Category("custom timer")]
        public Color ForeBackColor { get => foreBackColor; set { foreBackColor = value; this.Invalidate(); } }
        [Category("custom timer")]
        public int ChannelHeight { get => channelHeight; set { channelHeight = value; this.Invalidate(); } }
        [Category("custom timer")]
        public int SliderHeight { get => sliderHeight; set { sliderHeight = value; this.Invalidate(); } }
        [Category("custom timer")]
        public textPosition ShowValue { get => showValue; set { showValue = value; this.Invalidate(); } }

        [Category("custom timer")]
        [Browsable(true)]
        [EditorBrowsable(EditorBrowsableState.Always)]
        public override Font Font { get => base.Font; set => base.Font = value; }
        [Category("custom timer")]
        public override Color ForeColor { get => base.ForeColor; set => base.ForeColor = value; }

        protected override void OnPaintBackground(PaintEventArgs pevent)
        {
            Graphics graph = pevent.Graphics;
            Rectangle rectChannel = new Rectangle(0, 0, Width, channelHeight);
            using (var brushChannel = new SolidBrush(channelColor))
            {
                rectChannel.Y = (channelHeight >= sliderHeight) ? Height - channelHeight : Height - ((channelHeight + sliderHeight) / 2);
                graph.Clear(this.Parent.BackColor);
                graph.FillRectangle(brushChannel, rectChannel);
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e); // Ensure base class OnPaint is called for any base class drawing

            Graphics graph = e.Graphics;
            double scaleFactor = ((double)Value - Minimum) / ((double)Maximum - Minimum);
            int sliderWidth = (int)(scaleFactor * Width);
            Rectangle rectSlider = new Rectangle(0, 0, sliderWidth, sliderHeight);
            using (var brushSlider = new SolidBrush(sliderColor))
            {
                rectSlider.Y = (sliderHeight >= channelHeight) ? Height - sliderHeight : Height - ((sliderHeight + channelHeight) / 2);
                if (sliderWidth > 1) { graph.FillRectangle(brushSlider, rectSlider); }
                if (showValue != textPosition.none)
                {
                    drawValueText(graph, sliderWidth, rectSlider);
                }
            }
        }

        private void drawValueText(Graphics graph, int sliderWidth, Rectangle rectSlider)
        {
            string text = Value.ToString() + "%";
            var textSize = TextRenderer.MeasureText(text, Font);
            var rectText = new Rectangle(0, 0, textSize.Width, textSize.Height);
            using (var brushText = new SolidBrush(this.ForeColor))
            using (var brushTextBack = new SolidBrush(ForeBackColor))
            using (var textFormat = new StringFormat())
            {
                switch (showValue)
                {
                    case textPosition.left:
                        rectText.X = 0;
                        textFormat.Alignment = StringAlignment.Near;
                        break;
                    case textPosition.right:
                        rectText.X = Width - textSize.Width;
                        textFormat.Alignment = StringAlignment.Far;
                        break;
                    case textPosition.centered:
                        rectText.X = (Width - textSize.Width) / 2;
                        textFormat.Alignment = StringAlignment.Center;
                        break;
                    case textPosition.sliding:
                        rectText.X = sliderWidth - textSize.Width;
                        textFormat.Alignment = StringAlignment.Center;
                        using (var brushClear = new SolidBrush(Parent.BackColor))
                        {
                            var rect = rectSlider;
                            rect.Y = rectText.Y;
                            graph.FillRectangle(brushClear, rect);
                        }
                        break;
                }
                graph.FillRectangle(brushTextBack, rectText);
                graph.DrawString(text, Font, brushText, rectText, textFormat);
            }
        }
    }
}