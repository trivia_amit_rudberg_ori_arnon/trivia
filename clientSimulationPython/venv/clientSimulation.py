import socket
SERVER_IP = ""
SERVER_PORT = 0


def configServer():
    global SERVER_IP, SERVER_PORT
    try:
        f = open("config.txt", "r")
        data = f.read()
        SERVER_IP = data[data.index("server_ip=")+10:data.index("\n")]
        SERVER_PORT= int(data[data.index("port=")+5:data.index("\n", data.index("port="))])


        if (not( 1024 < SERVER_PORT < 65535 )):
             raise Exception("Sorry, the port need to be between 1024 to 65535")
        f.close()


    except FileNotFoundError:
        raise Exception("Config file 'config.txt' not found")
    except ValueError:
        f.close()
        raise Exception("Invalid port format in config file")
    except Exception as e:
        f.close()
        raise e

def recv(sock):
    server_msg = sock.recv(1024)
    server_msg = server_msg.decode()
    return  server_msg

def send(sock,msg):
     sock.sendall(msg.encode())

def login(sock):
    #send
    username=input("username: ")
    password=input("password: ")
    msgJOSN = '{"username": "'+username+'", "password":"'+ password +'"}'
    size = len(msgJOSN)
    status='101'
    msg = status + "00" + str(size) + msgJOSN
    send(sock,msg)
    print("sent to the server")

      #recv
    print(recv(sock))


def signup(sock):
    #send
    username=input("username: ")
    password=input("password: ")
    mail=input("mail: ")
    msgJOSN = '{"username": "'+username+'", "password": "'+ password +'", "email": "'+ mail+'"}'
    size = len(msgJOSN)
    status='102'
    msg = status+"00"+str(size)+msgJOSN
    send(sock,msg)
    print("sent to the server")


     #recv
    print(recv(sock))

def main():
    try:
        # Create a TCP/IP socket
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        configServer()

        # Connecting to remote computer
        server_address = (SERVER_IP, SERVER_PORT)
        sock.connect(server_address)

        #send
        while True:
            choose = input("1.signup\n2.login\n3.exit\n")
            if (choose == '1'):
                signup(sock)
            elif(choose == '2'):
                login(sock)
            else:
                break




        x = input()


        sock.close()
    except Exception as e:
        print("An error occurred:", e)
    finally:
        sock.close()

if __name__ == '__main__':
    main()
